export default [
  {
    title: 'Administrador',
    icon: 'HomeIcon',
    tag: '4',
    tagVariant: 'light-warning',
    children: [
      {
        title: 'Empresas',
        route: 'apps-empresa-list',
        action: 'read',
        resource: 'empresas',
        tag: '2',
        tagVariant: 'light-warning',
        children: [
          {
            title: 'Listado',
            route: 'apps-empresa-list',
            action: 'read',
            resource: 'empresas',
          },
          {
            title: 'Crear',
            route: 'apps-empresa-add',
            action: 'read',
            resource: 'empresas',
          },
        ],
      },

      {
        title: 'Modulos',
        route: 'apps-modulo-list',
        tag: '2',
        tagVariant: 'light-warning',
        children: [
          {
            title: 'Listado',
            route: 'apps-modulo-list',
          },
          {
            title: 'Crear',
            route: 'apps-modulo-add',
          },
        ],
      },
      {
        title: 'Roles',
        route: 'apps-role-list',
        action: 'read',
        resource: 'roles',
        tag: '2',
        tagVariant: 'light-warning',
        children: [
          {
            title: 'Listado',
            route: 'apps-role-list',
            action: 'read',
            resource: 'roles',
          },
          {
            title: 'Crear',
            route: 'apps-role-add',
            action: 'read',
            resource: 'roles',
          },
        ],
      },
      {
        title: 'Usuarios',
        route: 'apps-users-list',
        tag: '2',
        tagVariant: 'light-warning',
        children: [
          {
            title: 'Listado',
            route: 'apps-users-list',
          },
          {
            title: 'Crear usuario',
            route: 'apps-users-add',
          },
        ],
      },
      {
        title: 'Preguntas',
        route: 'apps-pregunta-list',
        action: 'read', 
        resource: 'preguntas',
        tag: '2',
        tagVariant: 'light-warning',
        children: [
          {
            title: 'Listado',
            route: 'apps-pregunta-list',

          },

        ],
      },
    ],
  },
]
