import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import ecommerceStoreModule from '@/views/apps/e-commerce/eCommerceStoreModule'
import empresaStoreModule from '@/views/apps/empresas/empresaStoreModule'
import referidoStoreModule from '@/views/apps/referidos/referidoStoreModule'
import app from './app'
import appConfig from './app-config'
import verticalMenu from './vertical-menu'
import userData from './user-data'


Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    appConfig,
    verticalMenu,
    referidoStoreModule,
    userData,
    'empresa': empresaStoreModule,
    'app-ecommerce': ecommerceStoreModule,
  },
  strict: process.env.DEV,
})
