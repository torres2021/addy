export const codeButton = `
<template>
  <div>
    <b-button
      variant="primary"
      disabled
      class="mr-1"
    >
      <b-spinner small />
      <span class="sr-only">Loading...</span>
    </b-button>

    <b-button
      variant="primary"
      disabled
      class="mr-1"
    >
      <b-spinner small />
      Loading...
    </b-button>

    <b-button
      variant="primary"
      disabled
      class="mr-1"
    >
      <b-spinner
        small
        type="grow"
      />
      <span class="sr-only">Loading...</span>
    </b-button>

    <b-button
      variant="primary"
      disabled
      class="mr-1"
    >
      <b-spinner
        small
        type="grow"
      />
      Loading...
    </b-button>
  </div>
</template>

<script>
import { BSpinner, BButton } from 'bootstrap-vue'

export default {
  components: {
    BButton,
    BSpinner,
  },
}
</script>
`
