import axios from '@axios'
const token = localStorage.getItem('accessToken');
const config = {
  headers: {
    'Contenct-type': 'multipart/form-data',
    'Authorization': `Bearer ${token}`,
    //'Content-Type': 'multipart/form-data'
  },
};
export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    fetchPreguntas(ctx, queryParams) {
      return new Promise((resolve, reject) => {
        axios
          .get('/apps/invoice/invoices', { params: queryParams })
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    fetchPregunta(ctx, { id }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`/apps/invoice/invoices/${id}`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    },
    delete() {
      return new Promise((resolve, reject) => {
        axios
          .get('/apps/invoice/clients')
          .then((response) => {
            console.log(response)
            resolve(response)
          })
          .catch(error => reject(error))
      })
    },

    listResponsePregunta(ctx, data){

      return new Promise((resolve, reject) => {
        axios
          .get('api/auth/preguntas/'+data.id+'/edit', {}, config)
          .then((response) => {
            console.log(response)
            resolve(response)
          })
          .catch(error => reject(error))
      })
    
    },

      addPregunta(ctx, data) {
       return new Promise((resolve, reject) => {
         axios
           .post('api/auth/respuestas/registro', data, config)
           .then(response => resolve(response))
           .catch(error => reject(error))
       })
     }, 
  },
}
