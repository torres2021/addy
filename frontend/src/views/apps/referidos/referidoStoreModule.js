import axios from '@axios'
const token = localStorage.getItem('accessToken');

const config = {
  headers: {
    'Contenct-type': 'multipart/form-data',
    'Authorization': `Bearer ${token}`,
    //'Content-Type': 'multipart/form-data'
  },
};
export default {
  namespaced: true,

  state: {
    referido:[],
  },
  getters: {
    
  },
  mutations: {
    SET_REFERIDOS: (state, payload) => {
      state.referido = payload;
  },
  },
  actions: {
    fetchInvoices(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get('api/auth/referidos', {}, config)
          .then(response => {
            ctx.commit('SET_REFERIDOS', response.data.data)
            resolve(response.data.data)
          })
          .catch(error => {
            reject(error)
          })
      })
    },



    fetchCreateReferido(ctx) {
      return new Promise((resolve, reject) => {
        axios
          .get('api/auth/referidos/create', {}, config)
          .then(response => {
            resolve(response.data)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    getReferidoForId(ctx, { id }) {
     return new Promise((resolve, reject) => {
        axios
          .get(`api/auth/referidos/${id}/edit`, {}, config)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    }, 

    updateReferido(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .put('api/auth/referidos/'+data.id_referido, data, config)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    fetchReferidosAdicional(ctx, data) {
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/referidos/adicional', data, config)
          .then(response => {

            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    addReferido(ctx, referido) {

      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/referidos', referido, config)
          .then(response => {
            console.log(response)
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },

    validateCampos(ctx, referido){
      return new Promise((resolve, reject) => {
        axios
          .post('api/auth/referidos/validacion', referido, config)
          .then(response => {
            console.log(response)
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    }
  },
}
