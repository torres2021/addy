<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController AS Login;
use App\Http\Controllers\EmpresasController AS Empresas;
use App\Http\Controllers\RolesController AS Roles;
use App\Http\Controllers\ReferidoController AS Referidos;
use App\Http\Controllers\PersonasController AS Personas;
use App\Http\Controllers\RespuestasController AS Respuestas;
use App\Http\Controllers\PreguntasController AS Preguntas;






/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::post('/login', [Login::class, 'login']);
Route::post('/signup', [Login::class, 'signup']);
Route::post('/recovery', [Login::class, 'recovery']);


Route::group(['prefix' => 'auth'], function () {
      
    Route::group(['middleware' => 'auth:api'], function() {
    Route::get('/logout', [Login::class, 'logout']);
    Route::get('/user', [Login::class, 'user']);

      /* modulo de empresas */
      Route::get('/empresas', [Empresas::class, 'index']);
      Route::post('/empresas', [Empresas::class, 'store']);

      Route::get('/empresas/ciudad/{codigo}', [Empresas::class, 'ciudad']);

      /*Modulo de roles */
      Route::resource('/roles', Roles::class);

      /*Modulo de referidos */
      Route::resource('/referidos', Referidos::class);

      /*Modulo de referidos*/
      Route::post('/referidos/adicional', [Referidos::class, 'informacion_adicional']);

      /*Validacion de referidos*/
      Route::post('/referidos/validacion', [Referidos::class, 'validacion']);
      
      /*Validacion de preguntas*/
      Route::post('/personas/perfil/validacion', [Personas::class, 'validacion']);
      
      /* Respuestas */
      Route::post('/cuestionarios/respuestas', [Respuestas::class, 'store']);
      

      /* Personas */
      
      /* --- Registro de perfil al haber nuevo registro */
      Route::post('/personas/perfil/update', [Personas::class, 'actualizacion_perfil']);
      
      /* -- Actualizacion de información de perfil al haber nuevo registro */
      Route::post('/personas/perfil', [Personas::class, 'edicion']);
      
      /* ============= Actualizacion de info adicional perfil al haber nuevo registro  ===================== */
      Route::post('/personas/perfil/adicional', [Personas::class, 'informacion_adicional']);

      /* ======================== Actualizar perfil por ID ================================= */
      Route::post('/personas/perfil/update/informacion', [Personas::class, 'actualizar_informacion_perfil']);

      /* ======================== Registro de preguntas ================================= */
      Route::post('/respuestas/registro', [Respuestas::class, 'store']);
      

      /* PREGUNTAS */
      Route::resource('/preguntas', Preguntas::class);     
      Route::get('/preguntas/informacion/{id}', [Preguntas::class, 'informacion_preguntas']);

      
    });
});
