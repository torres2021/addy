
@if (session('status_success'))
<div class="row match-height">
    <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
        <div class="card card-congratulation-medal">
            <div class="card-body">
                <div class="alert alert-success" role="alert">
                    {{ session('status_success') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if ($errors->any())
<div class="row match-height">
    <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
        <div class="card card-congratulation-medal">
            <div class="card-body alert-danger">
                <div class="alert" role="alert">
                    <p>
                    <h3><b>Ha ocurrido un error, por verifica e intenta nuevamente.</b></h3>
                    </p>
                    <ul>
                        @foreach($errors->all() as $error) 
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

