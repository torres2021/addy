{!! Form::open(['id' => 'formulario', 'route' => array('asignacion.update', $id), 'files' => false]) !!}
@csrf
@method('POST')
<select class="form-control" name="permisos[]" id="selector_permisos" required="" multiple="">
    @foreach($permisos as $permiso)
        @if(is_array($code_permisos) && in_array($permiso->id, $code_permisos))
            <option value="{{ $permiso->id }}" selected="">{{ $permiso->name }}</option>
        @else
            <option value="{{ $permiso->id }}" >{{ $permiso->name }}</option>
        @endif
    @endforeach
</select>

<br>
<br>
<center>
    <input type="submit" class="btn btn-success" value="Actualizar registros">
</center>
{!! Form::close() !!}

<script type="text/javascript">
    $("#selector_permisos").select2();
</script>