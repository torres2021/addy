
<form id="formulario_edicion" data-action="{{ route('roles.update', $rol[0]->id) }}">

    @csrf
     @method('PUT')

    <div class="form-group">
        <label><b>(*) </b>Nombre del rol:</label>
        <input type="text" class="form-control" name="nombre_rol" value="{{ $rol[0]->nombre }}" >

    </div>

    <div class="form-group">
        <label><b>(*) </b>Slug del rol:</label>
        <input type="text" class="form-control" name="slug_rol" value="{{ $rol[0]->slug }}">
    </div>

    <div class="form-group">
        <label><b>(*) </b>Descripción del rol:</label>
        <textarea class="form-control" name="descripcion_rol">{{ $rol[0]->descripcion }}</textarea>                                    
    </div>


    <div class="form-group">
        <label>Asignar a la empresa:</label>
        <select class="form-control" name="empresa_modulo[]" id="selector_empresa" multiple="multiple">
            @foreach($empresas as $empresa)
                @if(is_array($code_empresa) && in_array($empresa->id, $code_empresa))
                    <option value="{{ $empresa->id }}" selected="">{{ $empresa->nombre_empresa }}</option>
                @else
                    <option value="{{ $empresa->id }}">{{ $empresa->nombre_empresa }}</option>
                @endif
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label><b>(*) </b>Estado del rol:</label>
        <select class="form-control" name="estado_rol" value="{{ old('estado_rol') }}">
            <option value="">Seleccione un estado</option>
            @if($rol[0]->estado_rol == 1)
                <option value="1" selected="">Activo</option>
                <option value="2">Inactivo</option>
            @else
                <option value="1">Activo</option>
                <option value="2" selected="">Inactivo</option>
            @endif
            
        </select>
    </div>
    
</form>

<script>
    $("#selector").select2();
    $("#selector_ciudad").select2();
    $("#selector_empresa").select2();
</script>