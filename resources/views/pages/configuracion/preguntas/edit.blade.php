

{!! Form::open(['id' => 'formulario', 'route' => array('preguntas.update', $pregunta[0]->id), 'files' => false]) !!}

@csrf
@method('PUT')

<div class="form-group">
	<label><b>(*) Nombre usuario:</b></label>
	<input type="text" class="form-control" value="{{ $pregunta[0]->nombre_usuario }}" readonly="">
</div>

<div class="form-group">
	<label><b>(*) Descripción de la Pregunta:</b></label>
	<input type="text" class="form-control" name="pregunta" value="{{ $pregunta[0]->descripcion_preguntas }}" >
</div>

<div class="form-group">
	<label><b>ID Opcional:</b></label>
	<input type="text" class="form-control" name="codigo_opcional" value="{{ $pregunta[0]->id_opcional }}" >
</div>

<div class="form-group">
	<label><b>(*) Asignar a la empresa:</b></label>
	<select class="form-control" name="empresas" id="selector_empresa">
		@foreach($empresas as $empresa)
			@if($pregunta[0]->id_empresa == $empresa->indentificacion_tributaria)
				<option value="{{ $empresa->indentificacion_tributaria }}" selected="">{{ $empresa->indentificacion_tributaria }} - {{ $empresa->nombre_empresa }}</option>
			@else
				<option value="{{ $empresa->indentificacion_tributaria }}">{{ $empresa->indentificacion_tributaria }} - {{ $empresa->nombre_empresa }}</option>
			@endif
		@endforeach
	</select>  
</div>

<div class="form-group">
	<label><b>(*) Asignar pregunta al Módulo: </b></label>
	<select class="form-control" name="modulo" id="selector">
		@foreach($modulos as $modulo)
			@if($pregunta[0]->id_modulo == $modulo->slug_modulo)
				<option value="{{ $modulo->slug_modulo }}" selected="">{{ $modulo->nombre_modulo }}</option>
			@else
				<option value="{{ $modulo->slug_modulo }}">{{ $modulo->nombre_modulo }}</option>
			@endif
		@endforeach
	</select> 
</div>

<div class="form-group">
	<label><b></b>Fecha registro: </label>
	<input type="date" class="form-control" value="{{ $pregunta[0]->fecha_pregunta }}">
</div>


<div class="form-group">
	<label>Estado de la Pregunta:</label>
	<select class="form-control" name="estado">
		@if($pregunta[0]->estado_pregunta == 1)
		<option value="1" selected="">Activo</option>
		<option value="2">Inactivo</option>
		@else
		<option value="1">Activo</option>
		<option value="2" selected="">Inactivo</option>
		@endif
	</select>
</div>

{!! Form::close() !!}

<script>
    $("#selector").select2();
    $("#selector_empresa").select2();
</script>