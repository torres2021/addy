
<div class="form-group">
    <label><b>Logo de la empresa</b></label>
    <img src="{{ asset('storage/'.$empresa[0]->logo_empresa) }}" width="150px"> 
</div>

<div class="form-group">
    <label><b>Favicon Empresa</b></label>
    <img src="{{ asset('storage/'.$empresa[0]->favicon_empresa) }}" width="150px"> 
</div>

<div class="form-group">
    <div class="row">
        <div class="col-3">
            <label><b>Tipo de Identificación:</b></label>
            <select class="form-control"  name="code_tipo_documento" readonly="" disabled="">
                @if($empresa[0]->code_tipo_documento == '1')
                <option value="1" selected="">CC</option>
                <option value="2">NIT</option>
                <option value="3">RUT</option>
                @elseif($empresa[0]->code_tipo_documento == '2')
                <option value="1">CC</option>
                <option value="2" selected="">NIT</option>
                <option value="3">RUT</option>
                @else
                <option value="1">CC</option>
                <option value="2">NIT</option>
                <option value="3" selected="">RUT</option>
                @endif
            </select>
        </div>
        <div class="col-9">
            <label><b>Número de Identificación:</b></label>
            <input type="text" class="form-control" value="{{ $empresa[0]->indentificacion_tributaria }}" disabled="">
        </div>
    </div>
</div>

<div class="form-group">
    <label><b>Razón social:</b></label>
    <input type="text" class="form-control" value="{{ $empresa[0]->nombre_empresa }}" readonly="">
</div>

<div class="form-group">
    <label><b>Email empresa:</b></label>
    <input type="email" class="form-control" value="{{ $empresa[0]->email_empresa }}" readonly="">
</div>

<div class="form-group">
    <label><b>Descripción de la empresa:</b></label>
    <textarea class="form-control" readonly="">{{ $empresa[0]->descripcion_empresa }}</textarea>

</div>

<div class="form-group">
    <label><b>Acerca de su negocio:</b></label>
    <textarea class="form-control" readonly="">{{ $empresa[0]->codigo_postal }}</textarea>

</div>

<div class="form-group">
    <label><b>Teléfono fijo:</b></label>
    <input class='form-control'  value="{{ $empresa[0]->telefono_principal }}" disabled="">

</div>

<div class="form-group">
    <label><b>WhatsApp:</b></label><br>
    <input class='form-control' type="tel" value="{{ Crypt::decryptString($empresa[0]->telefono_whatsapp)}}" disabled="">
</div>

<div class="form-group">
    <label><b>Dirección de la empresa:</b></label>
    <input class="form-control" type="text" value="{{ $empresa[0]->direccion_empresa }}" readonly="">
</div>

<div class="form-group">
    <label><b>C&oacute;digo postal de la empresa:</b></label>
    <input class="form-control" type="number" value="{{ $empresa[0]->codigo_postal }}" readonly="">
</div>

<div class="form-group">
    <label><b>Palabras claves:</b></label>
    <div class="row">
        @foreach($palabras_claves as $palabra)
        <button type="button" class="btn btn-info btn-sm" style="margin: 2px;">{{ $palabra }}</button>
        @endforeach
    </div>
</div>

<div class="form-group">
    <label><b>País de la empresa:</b></label>
    <input class="form-control" type="text" value="{{ $pais[0]->pais }}" readonly="">
</div>

<div class="form-group">
    <label><b>Ciudad de la empresa:</b></label>
    <input class="form-control" type="text" value="{{ $ciudad[0]->nombre_ciudad }}" readonly="">
</div>

<div class="form-group">
    <label><b>Estado de la Empresa:</b></label>
    <select class="form-control" disabled="">
        @if($empresa[0]->estado_empresa == '1')
        <option value="1" selected="">Activo</option>
        <option value="2">Inactivo</option>
        @else
        <option value="2">Activo</option>
        <option value="2" selected="">Inactivo</option>
        @endif
    </select>
</div>


<script>
    $("#selector").select2();
    $("#selector_ciudad").select2();
    $("#selector_empresa").select2();
</script>