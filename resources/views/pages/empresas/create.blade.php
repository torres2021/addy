@extends('layouts.app')

@section('title', 'Registro Empresa - Aplicativo Addy')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            @include('pages.configuracion.mensajes.alertas')
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Registro de una empresa</h3>
                            <p class="card-text font-small-3">Bienvenido, desde esta sección puedes crear una nueva Empresa en Addy</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>

                            <form id="form_empresa" action="{{ route('empresas.store') }}" method="POST" enctype="multipart/form-data">

                                @csrf
                                @method('POST')

                                <div class="form-group">
                                    <label><b>(*) </b>Logo de la empresa</label>
                                    <input class='form-control' type="file" id="logo" name="logo_empresa"  value="{{ old('logo_empresa') }}">
                                </div>

                                <div class="form-group">
                                    <label><b>(*) </b>Favicon Empresa</label>
                                    <input class='form-control' type="file" id="favicon" name="favicon_empresa"  value="{{ old('favicon_empresa') }}">
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-3">
                                            <label><b>(*) </b>Tipo de Identificación:</label>
                                            <select class="form-control"  name="code_tipo_documento" >
                                                <option value=""></option>
                                                <option value="1">CC</option>
                                                <option value="2">NIT</option>
                                                <option value="3">RUT</option>
                                            </select>
                                        </div>
                                        <div class="col-7">
                                            <label><b>(*) </b>Número de Identificación:</label>
                                            <input type="text" class="form-control" name="indentificacion_tributaria" value="{{ old('indentificacion_tributaria') }}" id="nro_identificacion" >
                                        </div>
                                        <div class="col-2">
                                            <label><b>(*) </b>No. de verificación:</label>
                                            <input type="text" class="form-control" name="digito_verificacion" readonly=""  value="{{ old('digito_verificacion') }}" id="digito_verificacion" >
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><b>(*) </b>Razón social:</label>
                                    <input type="text" class="form-control" name="nombre_empresa"  value="{{ old('nombre_empresa') }}">
                                </div>

                                <div class="form-group">
                                    <label><b>(*) </b>Email empresa:</label>
                                    <input type="email" class="form-control" name="email_empresa" id="email"  value="{{ old('email_empresa') }}">
                                    <code id="error-email" style="display: none;">Debes de digitar un correo válido.</code>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label>Descripción de la empresa:</label>
                                            <textarea class="form-control" name="descripcion_empresa" >{{ old('descripcion_empresa') }}</textarea>
                                        </div>
                                        <div class="col">
                                            <label>Acerca de su negocio:</label>
                                            <textarea class="form-control" name="acerca_negocio"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label>Teléfono fijo:</label>
                                            <input class='form-control' type="number" name="telefono_fijo"  value="{{ old('telefono_fijo') }}">
                                        </div>
                                        <div class="col">
                                            <label>WhatsApp:</label><br>
                                            <input class='form-control' type="tel" name="telefono_whatsapp" id="celular_movil" value="{{ old('telefono_whatsapp') }}">
                                            <span id="valid-msg" class="hide">✓ Válido</span>
                                            <span id="error-msg" class="hide"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label>Dirección de la empresa:</label>
                                            <input class="form-control" type="text" name="direccion_empresa"  value="{{ old('direccion_empresa') }}">
                                        </div>
                                        <div class="col">
                                            <label>Código Postal:</label>
                                            <input class="form-control" type="number" name="codigo_postal"  value="{{ old('codigo_postal') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Palabras claves:</label>
                                    <select class="form-control palabras_claves" multiple="multiple" name="palabras_claves[]">

                                    </select>  
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label>País de la empresa:</label>
                                            <select class="form-control" id="selector"  name="codigo_pais" value="{{ old('codigo_pais') }}">
                                                <option value="" selected=""></option>
                                                @foreach($paises as $pais)
                                                <option value="{{ $pais->codigo }}">{{ $pais->pais }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label>Ciudad de la empresa:</label>
                                            <select class="form-control" id="selector_ciudad"  name="codigo_ciudad" value="{{ old('codigo_ciudad') }}">
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Estado de la Empresa:</label>
                                    <select class="form-control" name="estado_empresa" value="{{ old('estado_empresa') }}">
                                        <option value="">Seleccione un estado</option>
                                        <option value="1">Activo</option>
                                        <option value="2">Inactivo</option>
                                    </select>
                                </div>
                                <center>
                                    <button type="submit" class="btn btn-primary" id="registro_empresa">Registrar Empresa</button>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection