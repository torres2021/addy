@extends('layouts.app')

@section('title', 'Registro Empresa - Aplicativo Addy')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            @include('pages.configuracion.mensajes.alertas')
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Registro de agente</h3>
                            <p class="card-text font-small-3">Bienvenido, desde esta sección puedes registrar un Agente</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>

                            <form id="form_empresa" action="{{ route('agentes.store') }}" method="POST" enctype="multipart/form-data">

                                @csrf
                                @method('POST')

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) Tipo de Identificación:</b></label>
                                            <select class="form-control"  name="code_tipo_documento"  required="">
                                                <option value=""></option>
                                                <option value="1">CC</option>
                                                <option value="2">NIT</option>
                                                <option value="3">RUT</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label><b>(*) Número de Identificación:</b></label>
                                            <input type="number" class="form-control" name="indentificacion_tributaria" value="{{ old('indentificacion_tributaria') }}" id="nro_identificacion"  required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                        	 <label><b>(*) Primer nombre:</b></label>
                                            <input type="text" class="form-control" name="primer_nombre" value="{{ old('primer_nombre') }}" required="">
                                        </div>
                                        <div class="col">
                                            <label><b>Segundo nombre:</b></label>
                                            <input type="text" class="form-control" name="segundo_nombre" value="{{ old('segundo_nombre') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                        	 <label><b>(*) Primer Apellido:</b></label>
                                            <input type="text" class="form-control" name="primer_apellido" value="{{ old('primer_apellido') }}" required="">
                                        </div>
                                        <div class="col">
                                            <label><b>Segundo Apellido:</b></label>
                                            <input type="text" class="form-control" name="segundo_apellido" value="{{ old('segundo_apellido') }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                        	<label><b>(*) Email:</b></label>
                                    		<input type="email" class="form-control" name="email" id="email"  value="{{ old('email') }}" required="">
                                    		<code id="error-email" style="display: none;">Debes de digitar un correo válido.</code>
                                        </div>
                                        <div class="col">
                                            <label><b>(*) WhatsApp:</b></label><br>
                                            <input class='form-control' type="tel" name="telefono_whatsapp" id="celular_movil" value="{{ old('telefono_whatsapp') }}">
                                            <span id="valid-msg" class="hide">✓ Válido</span>
                                            <span id="error-msg" class="hide"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                	<label><b> (*) Empresa: </b></label>
                                	<select class="form-control" name="empresa" id="selector_permisos" required="">
                                        <option value="" selected="">Seleccionar empresa</option>
                                        
									    @foreach($empresas as $empresa)
									    	<option value="{{ $empresa->id }}">{{ $empresa->nombre_empresa }}</option>
									    @endforeach
									</select>
                                </div>

                                <div class="form-group">
                                    <label><b> (*) Estado: </b></label>
                                    <select class="form-control" name="estado" >
                                        <option value="" selected="">Seleccionar estado</option>
                                        <option value="1">Activo</option>
                                        <option value="2">Inactivo</option>

                                    </select>
                                </div>

                                <center>
                                    <button type="submit" class="btn btn-primary" id="registro_empresa">Registrar Agente</button>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection