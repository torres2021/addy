
{!! Form::open(['id' => 'formulario', 'route' => array('agentes.update', $agente[0]->cedula_agente), 'files' => false]) !!}

	@csrf
	@method('PUT')
	
	<div class="form-group">
		<label><b>(*) Tipo de Identificación:</b></label>
		<select class="form-control" name="code_tipo_documento" readonly="">
			@if($agente[0]->tipo_identificacion == 1)
			<option value="1" selected="">CC</option>
			<option value="2">NIT</option>
			<option value="3">RUT</option>
			@elseif($agente[0]->tipo_identificacion == 2)
			<option value="1">CC</option>
			<option value="2" selected="">NIT</option>
			<option value="3">RUT</option>
			@else
			<option value="1">CC</option>
			<option value="2">NIT</option>
			<option value="3" selected="">RUT</option>
			@endif
		</select>
	</div>

	<div class="form-group">
		<label><b>Número de Identificación:</b></label>
		<input type="number" class="form-control" name="indentificacion_tributaria" value="{{ $agente[0]->cedula_agente }}" readonly="">
	</div>

	<div class="form-group">
		<label><b>Nombres:</b></label>
		<input type="text" class="form-control" value="{{ $agente[0]->nombre_agente }}" name="nombres">
	</div>

	<div class="form-group">
		<label><b>Apellidos:</b></label>
		<input type="text" class="form-control" value="{{ $agente[0]->apellido_agente }}" name="apellidos">
	</div>

	<div class="form-group">
		<label><b>Email:</b></label>
		<input type="text" class="form-control" value="{{ $agente[0]->correo_agente }}" name="email">
	</div>

	<div class="form-group">
		<label><b>WhatsApp:</b></label>
		<input type="text" class="form-control" value="{{ $agente[0]->celular_agente }}" name="whatsapp">
	</div>

	<div class="form-group">
		<label><b>Empresa:</b></label><br>
		<select class="form-control" name="empresa">
		@foreach($empresas as $inmobiliaria)
			@if(is_array($empresa) && in_array($inmobiliaria->id, $empresa))
				<option value="{{ $inmobiliaria->id }}" selected="">{{ $inmobiliaria->nombre_empresa }}</option>
			@else
				<option value="{{ $inmobiliaria->id }}">{{ $inmobiliaria->nombre_empresa }}</option>
	        @endif
		@endforeach
		</select>
	</div>

	<div class="form-group">
		<label><b> (*) Estado: </b></label>
		<select class="form-control" name="estado">
			@if($agente[0]->estado_agente == 1)
			<option value="1" selected="">Activo</option>
			<option value="2">Inactivo</option>
			@else
			<option value="1">Activo</option>
			<option value="2" selected="">Inactivo</option>
			@endif
		</select>
	</div>

{!! Form::close() !!}