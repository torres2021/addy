@extends('layouts.app')

@section('title', 'Información Bitácora')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">

    	@include('pages.configuracion.mensajes.alertas')

        <section id="dashboard-ecommerce">
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <div class="row">
                            	<div class="col">
                            		<h3>Información detallada</h3>		
                            	</div>
                            	<div class="row col">
                            		<div class="form-group col-4">
                            			<label><b>Última acción realizada:</b></label>
                            			<select class="form-control accion_bitacora" data-bitacora="{{ $bitacoras[0]->codigo_bitacora }}">
                            			@foreach($acciones as $accion)
                            				@if(is_array($acciones_bitacora) && in_array($accion->codigo_accion, $acciones_bitacora))
                            					<option value="{{ $accion->codigo_accion }}" selected="">{{ $accion->descripcion_accion }}</option>
        									@else
                            					<option value="{{ $accion->codigo_accion }}">{{ $accion->descripcion_accion }}</option>
        									@endif
                                    	@endforeach
                            		</select>
                            		</div>
                            		<div class="form-group col-4">
                            			<label><b>Estado bitácora:</b></label>
                            		<select class="form-control btn btn-success">
                            			@foreach($estados as $estado)
                            				@if(is_array($estados_bitacora) && in_array($estado->id, $estados_bitacora))
                            					<option value="{{ $estado->id }}" selected="">{{ $estado->nombre_estado }}</option>
        									@else
                            					<option value="{{ $estado->id }}">{{ $estado->nombre_estado }}</option>
        									@endif
                                    	@endforeach
                            		</select>
                            		</div>	

                            	</div>
                            </div>
                            

                            <!--
                            <p class="card-text font-small-3">Hola! acá verás un listado con todos los referidos registrados.</p>-->
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>

                            <!--
                            <div class="panel-busqueda">
                                <div class="row">
                                    <div class="form-group col-12 col-md-9 col-lg-9 col-sm-9 col-xl-9 col-lg-9">
                                        <label><b>Búsqueda de Bitacoras:</b></label>
                                        <input type="text" class="form-control" name="busqueda_empresa" id="busqueda" data-modulo="empresas">
                                    </div>
                                    <div class="form-group col-12 col-md-3 col-lg-3 col-sm-3 col-xl-3 col-lg-3">
                                        <label></label>
                                        <button class="btn btn-primary btn-block waves-effect waves-float waves-light" tabindex="4">Búsqueda avanzada</button>
                                    </div>
                                </div>

                            </div>-->
                            <div class="row">
                                <div class="col">
                                    <h5>Referido asociado</h5>
                                    <hr>
                                    <p>
                                        @foreach($personas as $persona)
                                            @if($persona->cedula_persona == $bitacoras[0]->cedula_persona)
                                                <button type="button" class="btn btn-info informacion_referido" data-cedula="{{ $bitacoras[0]->cedula_persona }}"> 
                                                    {{ $persona->cedula_persona }} - {{  $persona->nombres_persona }} {{ $persona->apellidos_persona }}
                                                </button>
                                            @endif
                                        @endforeach
                                    </p>
                                </div>
                            	<div class="col">
                            		<h5>Descripción Bitácora</h5>
                            		<hr>
                            		<p>
                            			{{ $bitacoras[0]->descripcion_bitacora }}
                            		</p>
                            	</div>
                            	<div class="col">
                            		<h5>Fecha creación Bitácora</h5>
                            		<hr>
                            		<p>
                            			{{ $bitacoras[0]->fecha_bitacora }}
                            		</p>
                            	</div>
                            	<div class="col">
                            		<h5>Código Bitácora</h5>
                            		<hr>
                            		<p>
                            			<button type="button" class="btn btn-danger">
                            				{{ $bitacoras[0]->codigo_bitacora }}
                            			</button>
                            		</p>
                            	</div>
                            </div>

                        </div>
                    </div>
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h3>Historial</h3>      
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="container">
                                @foreach($historiales as $historial)
                                    <input type="text" class="form-control" readonly="" value="Fecha registro: {{ $historial->fecha_historial }}">
                                    <p class="btn"><i>{{ $historial->descripcion_historial }}</i></p>
                                    <hr>
                                @endforeach
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@include('pages.configuracion.mensajes.modales')

@endsection
