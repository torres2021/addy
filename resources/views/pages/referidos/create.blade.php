@extends('layouts.app')

@section('title', 'Registro Referido Cliente Comprador - Aplicativo Addy')

@section('content')

<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            @include('pages.configuracion.mensajes.alertas')
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Registrar un nuevo referido</h3>
                            <p class="card-text font-small-3">Bienvenido, desde esta sección puedes registrar una persona</p>
                            
                            <hr>
                            
                            <form action="{{ route('referidos.store') }}" method="POST" enctype="multipart/form-data">

                                @csrf
                                @method('POST')

                                <div class="form-group">
                                    <label><b> Foto: </b></label>
                                    <input type="file" class="form-control" name="foto_persona">
                                </div>
                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>* Tipo de Identificación:</b></label>
                                            <select class="form-control"  name="code_tipo_documento"  >
                                                <option value=""></option>
                                                <option value="1">CC</option>
                                                <option value="2">NIT</option>
                                                <option value="3">RUT</option>
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label><b>* Número de Identificación:</b></label>
                                            <input type="number" class="form-control" name="numero_cedula" value="{{ old('numero_cedula') }}" >
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                        	 <label><b>* Primer nombre:</b></label>
                                            <input type="text" class="form-control" name="primer_nombre" value="{{ old('primer_nombre') }}" >
                                        </div>
                                        <div class="col">
                                            <label><b>Segundo nombre:</b></label>
                                            <input type="text" class="form-control" name="segundo_nombre" value="{{ old('segundo_nombre') }}">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                        	 <label><b>* Primer Apellido:</b></label>
                                            <input type="text" class="form-control" name="primer_apellido" value="{{ old('primer_apellido') }}" >
                                        </div>
                                        <div class="col">
                                            <label><b>Segundo Apellido:</b></label>
                                            <input type="text" class="form-control" name="segundo_apellido" value="{{ old('segundo_apellido') }}">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label><b> Fecha de Nacimiento: </b></label>
                                    <input type="date" class="form-control" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}">
                                </div>
                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                        	<label><b>* Celular:</b></label>
                                    		<input type="number" class="form-control" name="celular" value="{{ old('celular') }}">
                                        </div>
                                        <div class="col">
                                            <label><b>* WhatsApp:</b></label><br>
                                            <input class='form-control' type="tel" name="whatsApp" id="celular_movil" value="{{ old('whatsApp') }}">
                                            <span id="valid-msg" class="hide">✓ Válido</span>
                                            <span id="error-msg" class="hide"></span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label><b> * Correo electrónico: </b></label>
                                    <input type="email" class="form-control" name="correo" value="{{ old('correo') }}">
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label>* País:</label>
                                            <select class="form-control" id="selector"  name="codigo_pais" value="{{ old('codigo_pais') }}" >
                                                <option value="" selected=""></option>
                                                @foreach($paises as $pais)
                                                @if($pais->codigo == 'Co')
                                                <option value="{{ $pais->codigo }}" selected="">{{ $pais->pais }}</option>
                                                @else
                                                <option value="{{ $pais->codigo }}">{{ $pais->pais }}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label>* Ciudad:</label>
                                            <select class="form-control" id="selector_ciudad"  name="codigo_ciudad" value="{{ old('codigo_ciudad') }}" >
                                                @foreach($ciudades as $ciudad)
                                                    @if($ciudad->id_ciudades == '12')
                                                        <option value="{{ $ciudad->id_ciudades }}" selected="">{{ $ciudad->nombre_ciudad }}</option>
                                                    @else
                                                        <option value="{{ $ciudad->id_ciudades }}">{{ $ciudad->nombre_ciudad }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <center>
                                    <button type="submit" class="btn btn-success">Registrar Formulario</button>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

    $(document).on("change", "#selector", function () {

        $.ajax({
            type: 'GET',
            url: '/empresas/ciudad/' + $(this).val(),
            success: function (data) {
                $("#selector_ciudad").html(data);
            }
        });
    });
</script>
@endsection
