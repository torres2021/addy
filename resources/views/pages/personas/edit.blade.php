@extends('layouts.app')

@section('title', 'Actualización de registro - Aplicativo Addy')

@section('content')
<div class="content-wrapper">
    <div class="content-header row">
    </div>
    <div class="content-body">
        <section id="dashboard-ecommerce">
            @include('pages.configuracion.mensajes.alertas')
            <div class="row match-height">
                <div class="col-12 col-sm-12 col-xl-12 col-lg-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h3>Actualizacion de una persona</h3>
                            <p class="card-text font-small-3">Bienvenido, desde esta sección puedes crear una nueva Empresa en Addy</p>
                            <!--<h3 class="mb-75 mt-2 pt-50">
                                    <a href="javascript:void(0);">$48.9k</a>
                            </h3>-->
                            <hr>

                            {!! Form::open(['route' => array('personas.update', $persona[0]->cedula_persona), 'files' => true]) !!}

                            @csrf
                            @method('PUT')

                                <div class="form-group">
                                    <label><b>(*) Foto:</b></label>
                                    <input type="file" class="form-control" name="foto_persona">
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) Tipo de Identificación:</b></label>
                                            <select class="form-control"  name="code_tipo_documento"  >
                                                @if($persona[0]->tipo_identificacion == 1)
                                                    <option value="1" selected="">CC</option>
                                                    <option value="2">NIT</option>
                                                    <option value="3">RUT</option>
                                                @elseif ($persona[0]->tipo_identificacion == 2)
                                                    <option value="1">CC</option>
                                                    <option value="2" selected="">NIT</option>
                                                    <option value="3">RUT</option>
                                                @elseif ($persona[0]->tipo_identificacion == 3)
                                                    <option value="1">CC</option>
                                                    <option value="2">NIT</option>
                                                    <option value="3" selected="">RUT</option>
                                                @else
                                                    <option value="1">CC</option>
                                                    <option value="2">NIT</option>
                                                    <option value="3">RUT</option>
                                                @endif

                                            </select>
                                        </div>
                                        <div class="col">
                                            <label><b>(*) Número de Identificación:</b></label>
                                            <input type="number" class="form-control" name="cedula_persona" value="{{ $persona[0]->cedula_persona }}" readonly="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                             <label><b>(*) Nombres:</b></label>
                                            <input type="text" class="form-control" name="primer_nombre" value="{{ $persona[0]->nombres_persona }}" >
                                        </div>
                                        <div class="col">
                                             <label><b>(*) Apellidos:</b></label>
                                            <input type="text" class="form-control" name="primer_apellido" value="{{ $persona[0]->apellidos_persona }}" >
                                        </div>
                                        <!--
                                        <div class="col">
                                            <label><b>Segundo nombre:</b></label>
                                            <input type="text" class="form-control" name="segundo_nombre" value="{{ old('segundo_nombre') }}">
                                        </div>-->
                                    </div>
                                </div>
                                <!--
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                             <label><b>(*) Primer Apellido:</b></label>
                                            <input type="text" class="form-control" name="primer_apellido" value="" >
                                        </div>
                                        <div class="col">
                                            <label><b>Segundo Apellido:</b></label>
                                            <input type="text" class="form-control" name="segundo_apellido" value="{{ old('segundo_apellido') }}">
                                        </div>
                                    </div>
                                </div>
                                -->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) Email:</b></label>
                                            <input type="email" class="form-control" name="email" id="email"  value="{{ $persona[0]->correo_persona }}" >
                                            <code id="error-email" style="display: none;">Debes de digitar un correo válido.</code>
                                        </div>
                                        <div class="col">
                                            <label><b>Telefono fijo:</b></label><br>
                                            <input class='form-control' type="tel" name="telefono_fijo" value="{{ $persona[0]->telefono_fijo }}" >
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>(*) Celular:</b></label>
                                            <input type="number" class="form-control" name="celular" value="{{ $persona[0]->celular_movil }}">
                                        </div>
                                        <div class="col">
                                            <label><b>(*) WhatsApp:</b></label><br>
                                            <input class='form-control' type="tel" name="whatsApp" id="celular_movil" value="{{ $persona[0]->celular_whatsapp }}">
                                            <span id="valid-msg" class="hide">✓ Válido</span>
                                            <span id="error-msg" class="hide"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label><b> C&oacute;digo postal:</b></label>
                                    <input type="number" class="form-control" name="codigo_postal" value="{{ $persona[0]->codigo_postal }}">
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label><b>Direcci&oacute;n</label>
                                            <input type="text" class="form-control" name="direccion"value="{{ $persona[0]->direccion_persona }}" >
                                        </div>
                                        <div class="col">
                                            <label><b> (*) Empresa: </b></label>
                                            <select class="form-control" name="empresa" id="selector_permisos" >
                                                @foreach($empresas as $empresa)
                                                    @if($persona[0]->nit_empresa == $empresa->id)
                                                        <option value="{{ $empresa->id }}" selected="">{{ $empresa->nombre_empresa }}</option>
                                                    @else
                                                        <option value="{{ $empresa->id }}">{{ $empresa->nombre_empresa }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label>(*) País de la empresa:</label>
                                            <select class="form-control" id="selector"  name="codigo_pais" >
                                                @foreach($paises as $pais)
                                                    @if($persona[0]->codigo_pais == $pais->codigo)
                                                        <option value="{{ $pais->codigo }}" selected="">{{ $pais->pais }}</option>
                                                    @else
                                                        <option value="{{ $pais->codigo }}">{{ $pais->pais }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col">
                                            <label>(*) Ciudad de la empresa:</label>
                                            <select class="form-control" id="selector_ciudad" name="codigo_ciudad">
                                                <option value="{{ $ciudad[0]['id_ciudades'] }}" selected="">
                                                    {{ $ciudad[0]['nombre_ciudad'] }}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label>(*) Asignar perfil:</label>
                                            <select class="form-control"  name="perfil">
                                                @if($persona[0]->id_perfil == 1)
                                                    <option value="1" selected="">Agente</option>
                                                @else

                                                @endif
                                            </select>
                                        </div>
                                        <div class="col">   
                                            <label>(*) Asignar usuario:</label>
                                            <select class="form-control"  name="usuario" value="{{ old('usuario') }}" id="selector_usuario" >
                                                @foreach($usuarios as $usuario)
                                                    @if($usuario->email == 'admin@admin.com')
                                                        <option value="" disabled="">{{ $usuario->name }}</option>
                                                    @else
                                                        @if($persona[0]->id_usuario == $usuario->id)
                                                        <option value="{{ $usuario->id }}" selected="">{{ $usuario->name }}</option>
                                                        @else
                                                        <option value="{{ $usuario->id }}">{{ $usuario->name }}</option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                


                                <div class="form-group">
                                    <label><b> (*) Estado: </b></label>
                                    <select class="form-control" name="estado">
                                        @if($persona[0]->estado_persona == 1)
                                            <option value="1" selected="">Activo</option>
                                            <option value="2">Inactivo</option>
                                        @else
                                            <option value="1">Activo</option>
                                            <option value="2" selected="">Inactivo</option>
                                        @endif
                                    </select>
                                </div>

                                <center>
                                    <button type="submit" class="btn btn-primary" id="registro_empresa">Actualizar información</button>
                                </center>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection