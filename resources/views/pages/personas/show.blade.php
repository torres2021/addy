
<div class="form-group">
    <label><b>Foto: </b></label>
    <img src="">
</div>

<div class="form-group">
    <label><b>Cédula/NIT: </b></label>
    <input type="text" class="form-control" value="{{ $persona[0]['cedula_persona'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>Nombre: </b></label>
    <input type="text" class="form-control" value="{{ $persona[0]['nombres_persona'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>Apellido: </b></label>
    <input type="text" class="form-control" value="{{ $persona[0]['apellidos_persona'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>Dirección: </b></label>
    <input type="text" class="form-control" value="{{ $persona[0]['direccion_persona'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>Correo: </b></label>
    <input type="text" class="form-control" readonly="" value="{{ $persona[0]['correo_persona'] }}">
</div>

<div class="form-group">
    <label><b>Telefono fijo: </b></label>
    <input type="text" class="form-control" value="{{ $persona[0]['telefono_fijo'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>Celular: </b></label>
    <input type="text" class="form-control" value="{{ $persona[0]['celular_movil'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>WhatsaApp: </b></label>
    <input type="text" class="form-control" value="{{ $persona[0]['celular_whatsapp'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>País: </b></label>
    <input type="text" class="form-control" value="{{ $pais[0]['pais'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>Ciudad: </b></label>
    <input type="text" class="form-control" value="{{ $ciudad[0]['nombre_ciudad'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>Código Postal: </b></label>
    <input type="text" class="form-control" value="{{ $persona[0]['codigo_postal'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>Empresa: </b></label>
    <input type="text" class="form-control" value="{{ $empresa[0]['nombre_empresa'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>Perfil: </b></label>
    @if($persona[0]['id_perfil'] == 1)
    <input class="form-control" type="text" value="Agente" readonly="">                   
    @else
    <input class="form-control" type="text" value="No asignado">                   
    @endif
</div>

<div class="form-group">
    <label><b>Usuario asignado: </b></label>
    <input type="text" class="form-control" value="{{ $usuario[0]['name'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>Estado: </b></label>
    @if($persona[0]['estado_persona'] == 1)
    <input class="form-control" type="text" value="Activo" readonly="">                   
    @else
    <input class="form-control" type="text" value="Inactivo">                   
    @endif
</div>