

{!! Form::open(['id' => 'formulario', 'route' => array('modulos.update', $modulo[0]->id_modulo), 'files' => false]) !!}

@csrf
@method('PUT')

<div class="form-group">
    <label><b>(*) </b>Icono del Módulo <a href="" title="¿Por qué debo colocar esto?"><i data-feather="help-circle"></i></a></label>
    <input class='form-control' type="text" name="icon_modulo"  value="{{ $modulo[0]['icon_modulo'] }}">
</div>

<div class="form-group">
    <div class="row">
        <div class="col">
            <label><b>(*) </b>Nombre del módulo:</label>
            <input type="text" class="form-control" name="nombre_modulo" value="{{ $modulo[0]['nombre_modulo'] }}" >
        </div>
        <div class="col">
            <label><b>(*) </b>Slug del Módulo:</label>
            <input type="text" class="form-control" value="{{ $modulo[0]['slug_modulo'] }}" disabled="">
        </div>
    </div>
</div>

<div class="form-group">
    <label><b>(*) </b>Descripción del Módulo:</label>
    <textarea class="form-control" name="descripcion_modulo">{{ $modulo[0]['descripcion_modulo'] }}</textarea>                                    
</div>

<div class="form-group">
    <label>Asignar módulo a la empresa:</label>
    <select class="form-control" name="empresa_modulo[]" value="{{ old('estado_empresa') }}" id="selector_empresa" multiple="multiple">
        @foreach($empresas as $empresa)
        @if (is_array($empresas_del_modulo) && in_array($empresa->id, $empresas_del_modulo)) { ?>
        <option value="{{ $empresa->id }}" selected="">{{ $empresa->indentificacion_tributaria }} - {{ $empresa->nombre_empresa }}</option>
        @else
        <option value="{{ $empresa->id }}">{{ $empresa->indentificacion_tributaria }} - {{ $empresa->nombre_empresa }}</option>
        @endif
        @endforeach
    </select>                                   
</div>

<div class="form-group">
    <div class="row">
        <div class="col">
            <label><b>(*) </b>Sección del Módulo:</label>
            <select class="form-control" name="seccion_modulo" value="{{ old('estado_empresa') }}">
                <option value="">Seleccione un estado</option>
                @if($modulo[0]->seccion_id == '1')
                    <option value="1" selected="">APPS & PAGES</option>
                    <option value="2">USER INTERFACE</option>
                @else
                    <option value="1">APPS & PAGES</option>
                    <option value="2" selected="">USER INTERFACE</option>
                @endif
            </select>
        </div>
        <div class="col">
            <label><b>(*) </b>Estado de lal Módulo:</label>
            <select class="form-control" name="estado_modulo" value="{{ old('estado_empresa') }}">
                <option value="">Seleccione un estado</option>
                @if($modulo[0]->estado_id == '1')
                <option value="1" selected="">Activo</option>
                <option value="2">Inactivo</option>
                @else
                <option value="1">Activo</option>
                <option value="2" selected="">Inactivo</option>
                @endif
            </select>
        </div>
    </div>
</div>

{!! Form::close() !!}

<script>
    $("#selector").select2();
    $("#selector_ciudad").select2();
    $("#selector_empresa").select2();
</script>