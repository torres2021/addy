
<div class="form-group">
    <label><b>Icono del M&odblac;dulo: </b></label>
    <i data-feather="{{ $modulo[0]['icon_modulo'] }}"></i>
</div>

<div class="form-group">
    <label><b>Nombre del M&oacute;dulo: </b></label>
    <input type="text" class="form-control" value="{{ $modulo[0]['nombre_modulo'] }}" readonly="">
</div>

<div class="form-group">
    <label><b>Descripción del M&oacute;dulo: </b></label>
    <textarea class="form-control" readonly="">{{ $modulo[0]['descripcion_modulo'] }}</textarea>
</div>

<div class="form-group">
    <label><b>Slug del M&oacute;dulo: </b></label>
    <textarea class="form-control" readonly="">{{ $modulo[0]['slug_modulo'] }}</textarea>
</div>

<div class="form-group">
    <label><b>Sección del Módulo: </b></label>
    <select class="form-control" disabled="">
        @if($modulo[0]['slug_modulo'] == 1)
        <option value="1" selected="">APPS & PAGES</option>
        <option value="2">USER INTERFACE</option>
        @else
        <option value="1">APPS & PAGES</option>
        <option value="2" selected="">USER INTERFACE</option>
        @endif
    </select>
</div>


<div class="form-group">
    <label><b>Estado del M&oacute;dulo: </b></label>
    @if($modulo[0]['estado_id'] == 1)
    <input class="form-control" type="text" value="Activo" readonly="">                   
    @else
    <input class="form-control" type="text" value="Inactivo">                   
    @endif
</div>
