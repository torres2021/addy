<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;

    protected $fillable = [
        'logo_empresa',
        'favicon_empresa',
        'code_tipo_documento',
        'indentificacion_tributaria',
        'nombre_empresa',
        'email_empresa',
        'descripcion_empresa',
        'telefono_principal',
        'telefono_whatsapp',
        'palabras_claves',
        'acerca_de_negocio',
        'codigo_pais',
        'codigo_departamento',
        'codigo_ciudad',
        'codigo_postal',
        'direccion_empresa',
        'estado_empresa',
    ];
}
