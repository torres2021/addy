<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyRespuesta extends Model
{
    use HasFactory;

    protected $fillable = [
        'descripcion_respuesta',
        'valor_respuesta',
        'estado_respuesta',
    ];
}
