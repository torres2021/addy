<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyHistoriale extends Model
{
    use HasFactory;

    protected $fillable = [
		'id',
		'descripcion_historial',
		'codigo_bitacora',
		'id_usuario',
		'fecha_historial',
    ];
}
