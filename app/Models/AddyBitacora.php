<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyBitacora extends Model
{
    use HasFactory;

    protected $fillable = [
		'codigo_bitacora',
		'descripcion_bitacora',
		'fecha_bitacora',
		'id_creador',
		'cedula_persona',
		'id_accion',
		'id_estado',
    ];
}
