<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddyReferido extends Model
{
    use HasFactory;

    protected $fillable = [
        'tipo_identificacion',
        'cedula_persona',
        'foto_persona',
        'primer_nombre',
        'segundo_nombre',
        'primer_apellido',
        'segundp_apellido',
        'fecha_nacimiento',
        'correo_persona',
        'telefono_fijo',
        'celular_movil',
        'celular_whatsapp',
        'direccion_persona',
        'codigo_ciudad',
        'codigo_pais',
        'codigo_postal',
        'estado_persona',
    ];
}
