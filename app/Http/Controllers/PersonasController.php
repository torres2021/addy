<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AddyPersona as Personas;
use App\Models\Empresa as Empresas;
use App\Models\AddyPaise as Paises;
use App\Models\AddyCiudade as Ciudades;
use App\Models\User as Usuarios;
use App\Permisos\Models\Role as Roles;
use Auth;
use Gate;
use Storage;
use File;
use DB;
//use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class PersonasController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Gate::authorize('haveaccess', 'permisos.habilitado.personas');

        $informacion = DB::table('role_user')
            ->select('role_user.role_id', 'role_user.user_id', 'users.id', 'users.name', 'roles.id', 'roles.nombre', 'roles.slug')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->where('user_id', auth()->user()->id)
            ->get();

        $usuarios_creadores = [];
        $acciones_bitacora = [];
        $estados_bitacora = [];

        if ($informacion[0]->slug == 'administrator') {
            $personas = Personas::where('id_perfil', 1)->get();
        } else {
            $personas = Personas::where('id_perfil', 1)->get();
        }

        return view('pages.personas.index', compact('personas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $paises = Paises::all();
        $ciudades = Ciudades::all();
        $usuarios = Usuarios::all();
        $empresas = Empresas::all();

        return view('pages.personas.create', compact('paises', 'ciudades', 'usuarios', 'empresas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [

            'code_tipo_documento'       => 'required',
            'cedula_persona'            => 'required|unique:addy_personas,cedula_persona',
            'primer_nombre'             => 'required',
            'primer_apellido'           => 'required',
            'fecha_nacimiento'          => 'required',
            'email'                     => 'required',
            'celular'                   => 'required',
            'whatsApp'                  => 'required',
            'empresa'                   => 'required',
            'codigo_pais'               => 'required',
            'codigo_ciudad'             => 'required',
            'perfil'                    => 'required',
            'usuario'                   => 'required',
            'estado'                    => 'required',
        ];

        $messages = [
            'code_tipo_documento.required'  => 'El Tipo de documento no puede quedar vacio.',
            'cedula_persona.required'       => 'Ingrese un número de cédula.',
            'cedula_persona.unique'         => 'El número de cédula ya se encuentra registrada.',
            'primer_nombre.required'        => 'Digite su primer nombre.',
            'primer_apellido.required'      => 'Digite el primer apellido.',
            'fecha_nacimiento.required'     => 'Digite fecha de nacimiento.',
            'email.required'                => 'El Email es necesario.',
            'celular.required'              => 'Coloque un celular.',
            'whatsApp.required'             => 'El número de WhatsApp es importante.',
            'empresa.required'              => 'Por favor, seleccione una empresa.',
            'codigo_pais.required'          => 'Por favor, seleccione un País.',
            'codigo_ciudad.required'        => 'Por favor, seleccione una ciudad.',
            'perfil.required'               => 'Por favor, seleccione el perfil a asignar.',
            'usuario.required'              => 'Por favor, seleccione un usuario a asignar.',
            'estado.required'               => 'Por favor, seleccione el estado de la persona.',
        ];





        if ($request->foto_persona != null || $request->foto_persona != '') {

            $ruleFoto = ['foto_persona' => 'mimes:jpg,jpeg,bmp,png|max:5000'];
            $messagesFoto = ['foto_persona.mimes' => 'La extensión del archivo no es componible.'];

            $this->validate($request, $ruleFoto, $messagesFoto);

            $directorio = 'personas/' . $request->cedula_persona;

            if (!File::exists($directorio)) {
                Storage::makeDirectory($directorio);
            }

            $logo = $request->file('foto_persona')->store($directorio);
        } else {
            $logo = '';
        }

        Personas::create([
            'cedula_persona'        =>  $request->cedula_persona,
            'tipo_identificacion'   =>  $request->code_tipo_documento,
            'foto_persona'          =>  $logo,
            'nombres_persona'       =>  $request->primer_nombre . " " . $request->segundo_nombre,
            'apellidos_persona'     =>  $request->primer_apellido . " " . $request->segundo_apellido,
            'fecha_nacimiento'      =>  $request->fecha_nacimiento,
            'correo_persona'        =>  $request->email,
            'telefono_fijo'         =>  $request->telefono_fijo,
            'celular_movil'         =>  $request->celular,
            'celular_whatsapp'      =>  $request->whatsApp,
            'direccion_persona'     =>  $request->direccion,
            'codigo_ciudad'         =>  $request->codigo_ciudad,
            'codigo_pais'           =>  $request->codigo_pais,
            'codigo_postal'         =>  $request->codigo_postal,
            'nit_empresa'           =>  $request->empresa,
            'id_perfil'             =>  $request->perfil,
            'id_usuario'            =>  $request->usuario,
            'estado_persona'        =>  $request->estado,
        ]);

        return redirect()->route('personas.index')->with('status_success', 'Se ha registrado la persona en nuestro sitema');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {

        Gate::authorize('haveaccess', 'permisos.ver.personas');

        $persona = Personas::where('cedula_persona', '=', $id)->get();

        $pais = Paises::where('codigo', '=', $persona[0]->codigo_pais)->get();
        $ciudad = Ciudades::where('id_ciudades', '=', $persona[0]->codigo_ciudad)->get();
        $usuario = Usuarios::where('id', '=', $persona[0]->id_usuario)->get();
        $empresa = Empresas::where('id', '=', $persona[0]->nit_empresa)->get();

        return view('pages.personas.show', compact('persona', 'pais', 'ciudad', 'usuario', 'empresa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edicion(Request $request)
    {
        
       // $persona = Personas::where('id_usuario', Crypt::decrypt($request->id_usuario))->get();
       $persona = Personas::where('id_usuario', $request->id_usuario)->get();
        $documentos =   DB::table('addy_detalle_documento_referidos')
                        ->join('tipo_documentos', 'addy_detalle_documento_referidos.id_documento', '=', 'tipo_documentos.id')
                        ->join('addy_personas', 'addy_detalle_documento_referidos.cedula_referidos_documento', '=', 'addy_personas.cedula_persona')
                        ->where('addy_detalle_documento_referidos.cedula_referidos_documento', $persona[0]->cedula_persona)
                        ->get();
        
        return response()->json([
            'informacion'      =>  $persona,
            'documentos'    =>  $documentos,
        ]);
            
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Gate::authorize('haveaccess', 'permisos.editar.personas');


        $rules = [
            'foto_persona'          => 'mimes:jpg,jpeg,bmp,png|max:5000',
            'code_tipo_documento'   => 'required',
            'cedula_persona'        => 'required',
            'primer_nombre'         => 'required',
            'primer_apellido'       => 'required',
            'email'                 => 'required',
            'celular'               => 'required',
            'whatsApp'              => 'required',
            'empresa'               => 'required',
            'codigo_pais'           => 'required',
            'codigo_ciudad'         => 'required',
            'perfil'                => 'required',
            'usuario'               => 'required',
            'estado'                => 'required',
        ];

        $messages = [
            'foto_persona.mimes'            => 'La foto de persona debe ser un archivo de imagen.',
            'code_tipo_documento.required'  => 'El Tipo de documento no puede quedar vacio.',
            'cedula_persona.required'       => 'Ingrese un número de cédula.',
            'primer_nombre.required'        => 'Digite su primer nombre.',
            'primer_apellido.required'      => 'Digite el primer apellido.',
            'email.required'                => 'El Email es necesario.',
            'celular.required'              => 'Coloque un celular.',
            'whatsApp.required'             => 'El número de WhatsApp es importante.',
            'empresa.required'              => 'Por favor, seleccione una empresa.',
            'codigo_pais.required'          => 'Por favor, seleccione un País.',
            'codigo_ciudad.required'        => 'Por favor, seleccione una ciudad.',
            'perfil.required'               => 'Por favor, seleccione el perfil a asignar.',
            'usuario.required'              => 'Por favor, seleccione un usuario a asignar.',
            'estado.required'               => 'Por favor, seleccione el estado de la persona.',
        ];

        $this->validate($request, $rules, $messages);

        if (isset($request->foto_persona)) {

            $directorio = 'personas/' . $request->cedula_persona;

            if (!File::exists($directorio)) {
                Storage::makeDirectory($directorio);
            }

            $logo = $request->file('foto_persona')->store($directorio);
        } else {

            $foto = Personas::where('cedula_persona', $id)->get();

            $logo = $foto[0]->foto_persona;
        }

        Personas::where('cedula_persona', $id)
            ->update([
                'tipo_identificacion'   => $request->code_tipo_documento,
                'foto_persona'          => $logo,
                'nombres_persona'       => $request->primer_nombre,
                'apellidos_persona'     => $request->primer_apellido,
                'correo_persona'        => $request->email,
                'telefono_fijo'         => $request->telefono_fijo,
                'celular_movil'         => $request->celular,
                'celular_whatsapp'      => $request->whatsApp,
                'direccion_persona'     => $request->direccion,
                'codigo_ciudad'         => $request->codigo_ciudad,
                'codigo_pais'           => $request->codigo_pais,
                'codigo_postal'         => $request->codigo_postal,
                'nit_empresa'           => $request->empresa,
                'id_perfil'             => $request->perfil,
                'id_usuario'            => $request->usuario,
                'estado_persona'        => $request->estado
            ]);


        return redirect()->route('personas.index')->with('status_success', 'Se ha actualizado la información en nuestro sitema');
    }

    public function perfil()
    {

        $usuario = Usuarios::where('id', auth()->user()->id)->get();

        if ($usuario[0]->email == 'admin@admin.com') {
            return true;
        } else {

            if (auth()->user()->estado_user == 3) {
                return "sin_registro";
            } else if (auth()->user()->estado_user == 1) {
                return view('pages.configuracion.usuarios.perfil_actualizado');
            } else {
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function validacion(Request $request)
    {
        $validator_cedula = Validator::make($request->all(), [
            'numero_cedula' => 'required|unique:addy_personas,cedula_persona',
        ]);

        if ($validator_cedula->fails()) {
            return response()->json(['error_validacion' => $validator_cedula->errors()->toJson(), 'code' => 400], 500);
        }



        $validator_celular = Validator::make($request->all(), [
            'celular' => 'required|unique:addy_personas,celular_movil',
        ]);

        if ($validator_celular->fails()) {
            return response()->json(['error_validacion' => $validator_celular->errors()->toJson(), 'code' => 400], 500);
        }



        $validator_whatsapp = Validator::make($request->all(), [
            'whatsApp' => 'required|unique:addy_personas,celular_whatsapp',
        ]);

        if ($validator_whatsapp->fails()) {
            return response()->json(['error_validacion' => $validator_whatsapp->errors()->toJson(), 'code' => 400], 500);
        }

        $validator_correo = Validator::make($request->all(), [
            'correo' => 'required|email|unique:addy_personas,correo_persona',
        ]);

        if ($validator_correo->fails()) {
            return response()->json(['error_validacion' => $validator_correo->errors()->toJson(), 'code' => 400], 500);
        }
    }

    public function actualizacion_perfil(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'numero_cedula'     => 'required|unique:addy_personas,cedula_persona',
            'primer_nombre'     => 'required',
            'primer_apellido'   => 'required',
            'celular'           => 'required|unique:addy_personas,celular_movil',
            'whatsApp'          => 'required|unique:addy_personas,celular_whatsapp',
            'correo'            => 'required|email',
        ]);



        if ($validator->fails()) {
            return response()->json(['error_validacion' => $validator->errors()->toJson(), 'code' => 400]);
        }

        $foto = "NN";

        
        Personas::create([
            'cedula_persona'        => $request->numero_cedula,
            'tipo_identificacion'   => $request->code_tipo_documento,
            'foto_persona'          => $foto,
            'primer_nombre'         => $request->primer_nombre,
            'segundo_nombre'        => $request->segundo_nombre,
            'primer_apellido'       => $request->primer_apellido,
            'segundo_apellido'      => $request->segundo_apellido,
            'fecha_nacimiento'      => $request->fecha_nacimiento,
            'correo_persona'        => $request->correo,
            'telefono_fijo'         => $request->telefono_fijo,
            'celular_movil'         => $request->celular,
            'celular_whatsapp'      => $request->whatsApp,
            'direccion_persona'     => "No registra",
            'codigo_ciudad'         => $request->codigo_ciudad,
            'codigo_pais'           => $request->codigo_pais,
            'codigo_postal'         => $request->codigo_postal,
            'id_perfil'             => "2",
            'id_usuario'            => $request->id,//Crypt::decrypt($request->id),
            'estado_persona'        => $request->estado_persona,
        ]);


        /* Return of ability for new user */


        if ($request->tipo_cliente == 'agentes') {

            $persona = Personas::where('cedula_persona', $request->numero_cedula)->get();

            DB::table('role_user')->where('user_id', '=', $persona[0]->id_usuario)->update([
                'role_id'   =>  '2'
            ]);


            return response()->json(['informacion' => 'Actualización exitosa.', 'code' => 200]);
            

        } else if ($request->tipo_cliente == 'socio_referidor') {

            $persona = Personas::where('cedula_persona', $request->numero_cedula)->get();

            DB::table('role_user')->where('user_id', '=', $persona[0]->id_usuario)->update([
                'role_id'   =>  '3'
            ]);
        } else if ($request->tipo_cliente == 'visitantes') {

            $persona = Personas::where('cedula_persona', $request->numero_cedula)->get();

            DB::table('role_user')->where('user_id', '=', $persona[0]->id_usuario)->update([
                'role_id'   =>  '4'
            ]);
        } else {
            return response()->json(['error_validacion' => "Debe elegir un tipo de cliente", 'code' => 400]);
        }

        $user = Usuarios::where('id', '=', $persona[0]->id_usuario)->get();

        $rol = DB::table('role_user')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $persona[0]->id_usuario)
            ->get();

        $user = Auth::user();

        $tokenResult = $user->createToken('Nuevo acceso por token Addy');
        $token = $tokenResult->token;

        if ($rol[0]->slug == 'administrator') {

            $ability = ['action'  => 'manage', 'subject' => 'all'];

            $info[] = [
                "id" => $token->id,
                "id_user"  => $user->id, //Crypt::encrypt($user->id),
                "fullName" => $user->name,
                "userName" => $user->name,
                "avatar" => $user->avatar,
                "email" => $user->email,
                "role" => $rol[0]->slug,
                "ability" => [$ability]
            ];
        } else {

            $permisos = DB::table('permission_role')
                ->select('permission_role.*', 'roles.*', 'permissions.*')
                ->join('roles', 'permission_role.role_id', '=', 'roles.id')
                ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
                ->where('roles.slug', $rol[0]->slug)
                ->get();

            $ability = [];

            foreach ($permisos as $value) {
                $ability[] = ['action'  =>  $value->slug_vue, 'subject' =>  $value->name_module];
            }

            $info[] = [
                "id" => $token->id,
                "id_user"  => $user->id, //Crypt::encrypt($user->id),
                "fullName" => $user->name,
                "userName" => $user->name,
                "avatar" => $user->avatar,
                "email" => $user->email,
                "role" => $rol[0]->slug,
                "ability" => $ability
            ];
        }

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString(),
            'userData' => $info
        ]);
    }
    
    public function informacion_adicional(Request $request)
    {

        Personas::where('cedula_persona', $request->numero_cedula)->update([
            'fecha_nacimiento'  => $request->fecha_nacimiento,
            'direccion_persona' => $request->direccion_persona,
            'codigo_postal'     => $request->codigo_postal,
        ]);


        $validator = Validator::make($request->all(), [
            'documento_cedula' => 'mimes:doc,docx,pdf,jpg,png,jpeg',
            'documento_rut' => 'mimes:doc,docx,pdf,jpg,png,jpeg',
        ]);
        

        if ($validator->fails()) {
            return response()->json(['error_validacion' => $validator->errors()->toJson(), 'code' => 400]);
        }

        

        $directorio = 'personas/' . $request->numero_cedula;

        if (!File::exists($directorio)) {
            Storage::makeDirectory($directorio);
        }

        
        if ($request->documento_cedula) {
            $cedula = $request->file('documento_cedula')->store($directorio);
        } else {
            $cedula = "Sin registro";
        }

        if ($request->documento_rut) {
            $rut = $request->file('documento_rut')->store($directorio);
        } else {
            $rut = "Sin registro";
        }

        DB::table('tipo_documentos')->insert([
            'nombre_documento'      => 'Cedula del persona ' . $request->numero_cedula,
            'descripcion_documento' => $cedula,
            'estado_documento'      => 'registrado',
            'fecha_creacion'        => date('Y-m-d H:i:s'),
            'fecha_actualizacion'   => date('Y-m-d H:i:s'),
            'estado_tipo_dto' =>  '1'
        ]);

        $documento  = DB::table('tipo_documentos')->orderby('id', 'DESC')->take('1')->get();

        DB::table('addy_detalle_documento_referidos')->insert([
            'cedula_referidos_documento'    => $request->numero_cedula,
            'id_documento'                  => $documento[0]->id,
        ]);
        

        /* Registro de RUT */

        DB::table('tipo_documentos')->insert([
            'nombre_documento'      => 'RUT del persona ' . $request->numero_cedula,
            'descripcion_documento' => $rut,
            'estado_documento'      => 'registrado',
            'fecha_creacion'        => date('Y-m-d H:i:s'),
            'fecha_actualizacion'   => date('Y-m-d H:i:s'),
            'estado_tipo_dto' =>  '1'
        ]);

        $documento  = DB::table('tipo_documentos')->orderby('id', 'DESC')->take('1')->get();

        DB::table('addy_detalle_documento_referidos')->insert([
            'cedula_referidos_documento'    => $request->numero_cedula,
            'id_documento'                  => $documento[0]->id,
        ]);


        /* retorno con nuevo ability */

       
        $persona = Personas::where('cedula_persona', $request->numero_cedula)->get();
        
        
        if ($request->tipo_cliente == 'agentes') {

            $persona = Personas::where('cedula_persona', $request->numero_cedula)->get();

            DB::table('role_user')->where('user_id', '=', $persona[0]->id_usuario)->update([
                'role_id'   =>  '2'
            ]);
        } else if ($request->tipo_cliente == 'socio_referidor') {

            $persona = Personas::where('cedula_persona', $request->numero_cedula)->get();

            DB::table('role_user')->where('user_id', '=', $persona[0]->id_usuario)->update([
                'role_id'   =>  '3'
            ]);
        } else if ($request->tipo_cliente == 'visitantes') {

            $persona = Personas::where('cedula_persona', $request->numero_cedula)->get();

            DB::table('role_user')->where('user_id', '=', $persona[0]->id_usuario)->update([
                'role_id'   =>  '4'
            ]);
        } else {
            return response()->json(['error_validacion' => "Debe elegir un tipo de cliente", 'code' => 400]);
        }

        
        $user = Usuarios::where('id', '=', $persona[0]->id_usuario)->get();

        $rol = DB::table('role_user')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $persona[0]->id_usuario)
            ->get();

        $user = Auth::user();

        $tokenResult = $user->createToken('Nuevo acceso por token Addy');
        $token = $tokenResult->token;


        if ($rol[0]->slug == 'administrator') {

            $ability = ['action'  => 'manage', 'subject' => 'all'];

            $info[] = [
                "id" => $token->id,
                "id_user"  => $user->id, //Crypt::encrypt($user->id),
                "fullName" => $user->name,
                "userName" => $user->name,
                "avatar" => $user->avatar,
                "email" => $user->email,
                "role" => $rol[0]->slug,
                "ability" => [$ability]
            ];
        } else {

            $permisos = DB::table('permission_role')
                ->select('permission_role.*', 'roles.*', 'permissions.*')
                ->join('roles', 'permission_role.role_id', '=', 'roles.id')
                ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
                ->where('roles.slug', $rol[0]->slug)
                ->get();

            $ability = [];

            foreach ($permisos as $value) {
                $ability[] = ['action'  =>  $value->slug_vue, 'subject' =>  $value->name_module];
            }

            $info[] = [
                "id" => $token->id,
                "id_user"  => $user->id, //Crypt::encrypt($user->id),
                "fullName" => $user->name,
                "userName" => $user->name,
                "avatar" => $user->avatar,
                "email" => $user->email,
                "role" => $rol[0]->slug,
                "ability" => $ability
            ];
        }

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString(),
            'userData' => $info
        ]);
    }

    public function actualizar_informacion_perfil(Request $request){
        
        $validator = Validator::make($request->all(), [
            'numero_cedula'     => 'required',
            'primer_nombre'     => 'required',
            'primer_apellido'   => 'required',
            'celular'           => 'required',
            'whatsApp'          => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->all(), 'code' => 400]);
        }


        $personas = Personas::where('celular_whatsapp', $request->whatsApp)->get();        
        
        foreach ($personas as $value) {
            if($value->cedula_persona != $request->numero_cedula){

                $validator_cedula = Validator::make($request->all(), [
                    'whatsApp' => 'required|unique:addy_personas,celular_whatsapp',
                    'telefono_fijo' => 'required|unique:addy_personas,celular_movil',
                ]);

                if ($validator_cedula->fails()) {
                    return response()->json(['error_validacion' => $validator_cedula->errors()->toJson(), 'code' => 400], 500);
                }
            }else{
                Personas::where('cedula_persona', $request->numero_cedula)->update([
                    'tipo_identificacion'   => $request->code_tipo_documento,
                    'primer_nombre' => $request->primer_nombre,
                    'segundo_nombre'    => $request->segundo_nombre,
                    'primer_apellido'   => $request->primer_apellido,
                    'segundo_apellido'  => $request->segundo_apellido,
                    'celular_movil'     =>  $request->celular,
                    'celular_whatsapp'  =>  $request->whatsApp,
                    'codigo_ciudad'     =>  $request->codigo_ciudad,
                    'codigo_pais'       =>  $request->codigo_pais,
                ]);


            }
        }


        $user = Usuarios::where('id', '=', $personas[0]->id_usuario)->get();

        $rol = DB::table('role_user')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->join('users', 'role_user.user_id', '=', 'users.id')
            ->where('role_user.user_id', $personas[0]->id_usuario)
            ->get();

        $user = Auth::user();

        $tokenResult = $user->createToken('Nuevo acceso por token Addy');
        $token = $tokenResult->token;

        if ($rol[0]->slug == 'administrator') {

            $ability = ['action'  => 'manage', 'subject' => 'all'];

            $info[] = [
                "id" => $token->id,
                "id_user"  =>  $user->id,//Crypt::encrypt($user->id),
                "fullName" => $user->name,
                "userName" => $user->name,
                "avatar" => $user->avatar,
                "email" => $user->email,
                "role" => $rol[0]->slug,
                "ability" => [$ability]
            ];
        } else {

            $permisos = DB::table('permission_role')
                ->select('permission_role.*', 'roles.*', 'permissions.*')
                ->join('roles', 'permission_role.role_id', '=', 'roles.id')
                ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
                ->where('roles.slug', $rol[0]->slug)
                ->get();

            $ability = [];

            foreach ($permisos as $value) {
                $ability[] = ['action'  =>  $value->slug_vue, 'subject' =>  $value->name_module];
            }

            $info[] = [
                "id" => $token->id,
                "id_user"  =>  $user->id,//Crypt::encrypt($user->id),
                "fullName" => $user->name,
                "userName" => $user->name,
                "avatar" => $user->avatar,
                "email" => $user->email,
                "role" => $rol[0]->slug,
                "ability" => $ability
            ];
        }

        $personas = Personas::where('cedula_persona', $request->numero_cedula)->get();        

        foreach ($personas as $value) {
            # code...
            $info[1] = $value;
          }

        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString(),
            'userData' => $info
        ]);
    }
}
