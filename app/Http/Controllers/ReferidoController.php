<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AddyReferido AS Referidos;
use App\Models\AddyPaise AS Paises;
use App\Models\AddyCiudade AS Ciudades;
use App\Models\AddyFormulario AS Formularios;
use App\Models\AddyPersona AS Personas;

use App\Models\AddyTipoCliente AS Tipo_clientes;

use Gate;
use File;
use Storage;
use DB;
use Illuminate\Support\Facades\Validator;
//use Illuminate\Support\Facades\Crypt;

class ReferidoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        
        $informacion = DB::table('role_user')
                ->select('role_user.role_id', 'role_user.user_id', 'users.id', 'users.name', 'roles.id', 'roles.nombre', 'roles.slug')
                ->join('users', 'role_user.user_id', '=', 'users.id')
                ->join('roles', 'role_user.role_id', '=', 'roles.id')
                ->where('user_id', auth()->user()->id)
                ->get();

        $informacion_referidos = [];
        $tipo_clientes = [];
        
        if ($informacion[0]->slug == 'administrator') {
            
            $referidos = DB::table('addy_referidos_personas')
                        ->select('addy_referidos.*', 'addy_referidos_personas.*', 'addy_personas.cedula_persona as persona_cedula', 'addy_tipo_clientes.*', 'addy_referidos.id AS id_referido_primary')
                        ->join('addy_referidos', 'addy_referidos_personas.id_referido', '=', 'addy_referidos.cedula_persona')
                        ->join('addy_personas', 'addy_referidos_personas.id_cedula', '=', 'addy_personas.cedula_persona')
                        ->join('addy_tipo_clientes', 'addy_referidos_personas.id_tipo_clientes','=','addy_tipo_clientes.id')
                        ->get();

            
            
        } else {

            $persona = Personas::where('id_usuario', auth()->user()->id)->get();

            $referidos  = DB::table('addy_referidos_personas')
                        ->select('addy_referidos.*', 'addy_referidos_personas.*', 'addy_personas.cedula_persona as persona_cedula', 'addy_tipo_clientes.*', 'addy_referidos.id AS id_referido_primary')
                        ->join('addy_referidos', 'addy_referidos_personas.id_referido', '=', 'addy_referidos.cedula_persona')
                        ->join('addy_personas', 'addy_referidos_personas.id_cedula', '=', 'addy_personas.cedula_persona')
                        ->join('addy_tipo_clientes', 'addy_referidos_personas.id_tipo_clientes','=','addy_tipo_clientes.id')
                        ->where('addy_referidos_personas.id_cedula', '=', $persona[0]->cedula_persona)
                        ->get();

        }

        return response()->json([
            "success" => true,
            "message" => "Listado de referidos",
            "data" => ['referidos' => $referidos]
        ]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create() {
        if (auth()->user()->estado_user == 3) {

        } else {
            $tipo_clientes = Tipo_clientes::all();
            $ciudades = Ciudades::all();
            $paises = Paises::all();

            return response()->json([
                        "success" => true,
                        "message" => "Creacion de referidos",
                        "tipo_cliente"  => $tipo_clientes,
                        "data" => ['ciudades' => $ciudades, 'paises'  => $paises]
            ]);

        }
    }
    
    /**
     * Store a newly created resource in storage ok.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'numero_cedula' => 'required|int|unique:addy_referidos,cedula_persona',
            'primer_nombre' => 'required',
            'primer_apellido' => 'required',
            'celular' => 'unique:addy_referidos,celular_movil',
            'whatsApp' => 'required|unique:addy_referidos,celular_whatsapp',
            'correo' => 'required|email|unique:addy_referidos,correo_persona',
            'codigo_pais' => 'required',
            'codigo_ciudad' => 'required|int',
            'estado_persona'    => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['error_validacion' => $validator->errors()->toJson(), 'code' => 400]);
        }

        /*
        if (isset($request->foto_persona)) {
            
            $directorio = 'referidos/' . $request->numero_cedula;
            
            if (!File::exists($directorio)) {
                Storage::makeDirectory($directorio);
            }
            
            $foto = $request->file('foto_persona')->store($directorio);
            
        } else {
            $foto = "NN";
        }
        */
        
        $foto = "NN";
        
        Referidos::create([
            'tipo_identificacion'   => $request->code_tipo_documento,
            'cedula_persona'        => $request->numero_cedula,
            'foto_persona'          => $foto,
            'primer_nombre'         => $request->primer_nombre,
            'segundo_nombre'        => $request->segundo_nombre,
            'primer_apellido'       => $request->primer_apellido,
            'segundo_apellido'      => $request->segundo_apellido,
            'fecha_nacimiento'      => $request->fecha_nacimiento,
            'correo_persona'        => $request->correo,
            'telefono_fijo'         => "0",
            'celular_movil'         => $request->celular,
            'celular_whatsapp'      => $request->whatsApp,
            'direccion_persona'     => "No registra",
            'codigo_ciudad'         => $request->codigo_ciudad,
            'codigo_pais'           => $request->codigo_pais,
            'codigo_postal'         => "0",
            'estado_persona'        => $request->estado_persona,
        ]);
        

        $personas = Personas::where('id_usuario',$request->id_usuario)->get();

        DB::table('addy_referidos_incompletos')->insert([
            'id_referido'   => $request->numero_cedula,
            'nit_empresa'   => '900534143',
        ]);
        
        DB::table('addy_referidos_personas')->insert([
            'id_referido' => $request->numero_cedula,
            'id_cedula'   => $personas[0]->cedula_persona,
            'id_tipo_clientes'  =>  $request->tipo_cliente,
        ]);
        /*
        DB::table('addy_detalle_tipo_clientes')->insert([
            'cedula_referidos_tipo' => $request->numero_cedula,
            'id_tipo_clientes'      => $request->tipo_cliente,
        ]);
        */
        return response()->json([
            "status" => 200,
            "message" => "Registro exitoso"
        ]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
        $referido  = DB::table('addy_referidos_personas')
                        ->select('addy_referidos.*', 'addy_referidos_personas.*', 'addy_personas.cedula_persona as persona_cedula', 'addy_tipo_clientes.*', 'addy_referidos.id AS id_referido_primary')
                        ->join('addy_referidos', 'addy_referidos_personas.id_referido', '=', 'addy_referidos.cedula_persona')
                        ->join('addy_personas', 'addy_referidos_personas.id_cedula', '=', 'addy_personas.cedula_persona')
                        ->join('addy_tipo_clientes', 'addy_referidos_personas.id_tipo_clientes','=','addy_tipo_clientes.id')
                        ->where('addy_referidos_personas.id_referido', '=', $id)
                        ->get();

        return response()->json([
            "status" => 200,
            "message" => "Información del usuario",
            "data"      => $referido,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {


       $referido  = DB::table('addy_referidos_personas')
                        ->select('addy_referidos.*', 'addy_referidos_personas.*', 'addy_personas.cedula_persona as persona_cedula', 'addy_tipo_clientes.*', 'addy_referidos.id AS id_referido_primary')
                        ->join('addy_referidos', 'addy_referidos_personas.id_referido', '=', 'addy_referidos.cedula_persona')
                        ->join('addy_personas', 'addy_referidos_personas.id_cedula', '=', 'addy_personas.cedula_persona')
                        ->join('addy_tipo_clientes', 'addy_referidos_personas.id_tipo_clientes','=','addy_tipo_clientes.id')
                        ->where('addy_referidos.id', $id)
                        ->get();

        return response()->json([
            "status" => 200,
            "message" => "Información del usuario",
            "data"      => ['referidos' => $referido],
        ]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        
        $referidos = Referidos::where('id', $id)->get();
        
        foreach ($referidos as $value) {
            if($value->cedula_persona != $request->numero_cedula){

                $validator_cedula = Validator::make($request->all(), [
                    'whatsApp' => 'required|unique:addy_referidos,celular_whatsapp',
                    'telefono_fijo' => 'required|unique:addy_referidos,celular_movil',
                ]);

                if ($validator_cedula->fails()) {
                    return response()->json(['error_validacion' => $validator_cedula->errors()->toJson(), 'code' => 400], 500);
                }

            }else{
                
                Referidos::where('id', $id)->update([
                    'tipo_identificacion'   => $request->code_tipo_documento,
                    'primer_nombre' => $request->primer_nombre,
                    'segundo_nombre'    => $request->segundo_nombre,
                    'primer_apellido'   => $request->primer_apellido,
                    'segundo_apellido'  => $request->segundo_apellido,
                    'celular_movil'     =>  $request->celular,
                    'celular_whatsapp'  =>  $request->whatsApp,
                    'codigo_ciudad'     =>  $request->codigo_ciudad,
                    'codigo_pais'       =>  $request->codigo_pais,
                ]);

            }
        }

        return response()->json([
            "status" => 200,
            "message" => "Actualización exitosa"
        ]); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
    /*
    public function tipo_clientes($id) {

        Gate::authorize('haveaccess', 'permisos.crear.referidos');

        if (auth()->user()->estado_user == 3) {
            return redirect()->route('perfil.usuario');
        } else {

            if (isset($id)) {
                if ($id != 'cliente_comprador' AND $id != 'cliente_vendedor' AND $id != 'cliente_arrendatario' AND $id != 'cliente_arrendador') {
                    return "<div class='alert alert-danger'>Opción inválida.</div>";
                } else {

                    if ($id == 'cliente_comprador') {
                        $paises = Paises::all();

                        return view('pages.referidos.tipo_clientes.cliente_comprador', compact('paises'));
                    } else if ($id == 'cliente_vendedor') {

                        $paises = Paises::all();
                        return view('pages.referidos.tipo_clientes.cliente_vendedor', compact('paises'));
                    } else if ($id == 'cliente_arrendatario') {
                        return view('pages.referidos.tipo_clientes.cliente_arrendatario');
                    } else if ($id == 'cliente_arrendador') {
                        return view('pages.referidos.tipo_clientes.cliente_arrendador');
                    } else {
                        return "<div class='alert alert-danger'>Opción inválida.</div>";
                    }
                }
            }
        }
    }
    */

    public function informacion_adicional(Request $request){

            Referidos::where('cedula_persona', $request->numero_cedula)->update([
                'fecha_nacimiento'  => $request->fecha_nacimiento,
                'direccion_persona' => $request->direccion_persona,
                'codigo_ciudad'     => $request->codigo_ciudad,
                'codigo_pais'       => $request->codigo_pais,
                'codigo_postal'     => $request->codigo_postal, 
                'estado_persona'    => $request->estado_persona,
            ]);
            

                $validator = Validator::make($request->all(), [
                    'documento_cedula' => 'mimes:doc,docx,pdf,jpg,png,jpeg',
                    'documento_rut' => 'mimes:doc,docx,pdf,jpg,png,jpeg',
                ]);
                
                if ($validator->fails()) {
                    return response()->json(['error_validacion' => $validator->errors()->toJson(), 'code' => 400]);
                }
                
                 
                $directorio = 'referidos/' . $request->numero_cedula;
                
                if (!File::exists($directorio)) {
                    Storage::makeDirectory($directorio);
                }
                
                if($request->documento_cedula){
                    $cedula = $request->file('documento_cedula')->store($directorio);
                }else{
                    $cedula = "Sin registro";
                }
                
                if($request->documento_rut){
                    $rut = $request->file('documento_rut')->store($directorio);
                }else{
                    $rut = "Sin registro";
                }
                
                DB::table('tipo_documentos')->insert([
                    'nombre_documento'      => 'Cedula del referido '.$request->numero_cedula,
                    'descripcion_documento' => $cedula,
                    'estado_documento'      => 'registrado',
                    'fecha_creacion'        => date('Y-m-d H:i:s'),
                    'fecha_actualizacion'   => date('Y-m-d H:i:s'),
                    'estado_tipo_dto' =>  '1'
                ]);

                $documento  = DB::table('tipo_documentos')->orderby('id','DESC')->take('1')->get();

                DB::table('addy_detalle_documento_referidos')->insert([
                    'cedula_referidos_documento'    => $request->numero_cedula,
                    'id_documento'                  => $documento[0]->id,
                ]);

                /* Registro de RUT */

                DB::table('tipo_documentos')->insert([
                    'nombre_documento'      => 'RUT del referido '.$request->numero_cedula,
                    'descripcion_documento' => $rut,
                    'estado_documento'      => 'registrado',
                    'fecha_creacion'        => date('Y-m-d H:i:s'),
                    'fecha_actualizacion'   => date('Y-m-d H:i:s'),
                    'estado_tipo_dto' =>  '1'
                ]);
                
                $documento  = DB::table('tipo_documentos')->orderby('id','DESC')->take('1')->get();

                DB::table('addy_detalle_documento_referidos')->insert([
                    'cedula_referidos_documento'    => $request->numero_cedula,
                    'id_documento'                  => $documento[0]->id,
                ]);
            
            
            return response()->json([
                "status" => 200,
                "message" => "Actualización exitosa"
            ]);    
    }

    public function validacion(Request $request){

        $validator_cedula = Validator::make($request->all(), [
            'numero_cedula' => 'required|unique:addy_referidos,cedula_persona',
        ]);

        if ($validator_cedula->fails()) {
            return response()->json(['error_validacion' => $validator_cedula->errors()->toJson(), 'code' => 400], 500);
        }


        $validator_celular = Validator::make($request->all(), [
            'celular' => 'required|unique:addy_referidos,celular_movil',
        ]);

        if ($validator_celular->fails()) {
            return response()->json(['error_validacion' => $validator_celular->errors()->toJson(), 'code' => 400], 500);
        }
        
        

        $validator_whatsapp = Validator::make($request->all(), [
            'whatsApp' => 'required|unique:addy_referidos,celular_whatsapp',
        ]);
        
        if ($validator_whatsapp->fails()) {
            return response()->json(['error_validacion' => $validator_whatsapp->errors()->toJson(), 'code' => 400], 500);
        }



        $validator_correo = Validator::make($request->all(), [
            'correo' => 'required|email|unique:addy_referidos,correo_persona',
        ]);

        if ($validator_correo->fails()) {
            return response()->json(['error_validacion' => $validator_correo->errors()->toJson(), 'code' => 400], 500);
        }
    }

}
