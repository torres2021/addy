<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\AddyPersona AS Personas;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Password AS Password;

class AuthController extends Controller {

    public function signup(Request $request) {
      
        /* ES UN VALIDADOR */

        
        $validator = Validator::make($request->all(), [
          'username' => 'required|string|unique:users,name',
          'email' => 'required|string|email|unique:users,email',
          'password' => 'required|string|confirmed',
        ]);
        
        if ($validator->fails()) {
          return response()->json(['message' => $validator->errors()->all(), 'code' => 400]);
        }
        
        /* registro de usuario nuevo */
        $user = new User([
          'name' => $request->username,
          'email' => $request->email,
          'password' => bcrypt($request->password),
        ]);
        
        $user->save();
      
        $usuario = User::all()->last();
        
        DB::table('role_user')->insert([
          'role_id' => '4',
          'user_id'   => $usuario->id
        ]);
        /* fin registro de usuario nuevo */
        

        /* Iniciar sesion */
        $credentials = ['email'  =>  $request->email,   'password' =>  $request->password];

        if (!Auth::attempt($credentials)) {
          return response()->json([
            'message' => 'Credenciales incorrectas',
            'status' => 500 ]);
          }        

          $rol = DB::table('role_user')
          ->join('roles', 'role_user.role_id', '=', 'roles.id')
          ->join('users', 'role_user.user_id', '=', 'users.id')
          ->where('role_user.user_id', $usuario->id)
          ->get();


        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        

          if($rol[0]->slug == 'administrator'){

            $ability = ['action'  => 'manage',
            'subject' => 'all'];

              $info[] = ["id" => $token->id,
              "id_user"  => $user->id, //Crypt::encrypt($user->id),
              "fullName" => $user->name,
              "userName" => $user->name,
              "avatar" => $user->avatar,
              "email" => $user->email,
              "role" => $rol[0]->slug,
              "ability" => [$ability]
            ];
          }else{

            $permisos = DB::table('permission_role')
            ->select('permission_role.*', 'roles.*', 'permissions.*')
            ->join('roles', 'permission_role.role_id', '=', 'roles.id')
            ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
            ->where('roles.slug', $rol[0]->slug)
            ->get();
            
            $ability = [];

            foreach ($permisos as $value) {
              $ability[] = ['action'  =>  $value->slug_vue, 'subject' =>  $value->name_module];
            }

              $info[] = [
                "id" => $token->id,
                "id_user"  =>  $user->id,//Crypt::encrypt($user->id),
                "fullName" => $user->name,
                "userName" => $user->name,
                "avatar" => $user->avatar,
                "email" => $user->email,
                "role" => $rol[0]->slug,
                "ability" => $ability
              ];
          }
        
        $token->save();

        return response()->json([
          'access_token' => $tokenResult->accessToken,
          'token_type' => 'Bearer',
          'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString(),
          'userData' => $info
        ]);
    }
   //Hola como estas
    public function login(Request $request) {

        $request->validate([
          'email' => 'required|string|email',
          'password' => 'required|string',
          'remember_me' => 'boolean',
        ]);

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials)) {
          return response()->json([
            'message' => 'Credenciales incorrectas', 'status' => 401], 500);
          }

          $user = $request->user();

          $rol = DB::table('role_user')
          ->join('roles', 'role_user.role_id', '=', 'roles.id')
          ->join('users', 'role_user.user_id', '=', 'users.id')
          ->where('role_user.user_id', $user->id)
          ->get();
           
//hola

          $tokenResult = $user->createToken('Personal Access Token');
          $token = $tokenResult->token;

        if($rol[0]->slug == 'administrator'){

          $ability = ['action'  => 'manage',
          'subject' => 'all'];

            $info[] = ["id" => $token->id,
            "id_user"  => $user->id, //Crypt::encrypt($user->id),
            "fullName" => $user->name,
            "userName" => $user->name,
            "avatar" => $user->avatar,
            "email" => $user->email,
            "role" => $rol[0]->slug,
            "ability" => [$ability]
          ];

          /* Cambiar a

          $info[] = ["id" => $token->id,
            "id_user"  =>  Crypt::encrypt($user->id),
            "fullName" => $user_data->primer_nombre,
            "pnombre" => $user_data->primer_nombre,
            "papellido" => $user_data->primer_apellido,
            "whatsapp" => $user_data->celular_whatsapp,
            "telefono" => $user_data->celular_movil,
            "email" => $user_data->correo_persona,
            "ciudad" => $user_data->codigo_ciudad,
            "pais" => $user_data->codigo_pais,
            "postalcode" => $user_data->codigo_postal,
            "segnombre" => $user_data->segundo_nombre,
            "segapellido" => $user_data->segundo_apellido,
            "userName" => $user->name,
            "avatar" => $user->avatar,
            "email" => $user->email,
            "role" => $rol[0]->slug,
            "ability" => [$ability]

            */

            $personas = Personas::where('id_usuario', $user->id)->get();
            
          }else{
             
              $permisos = DB::table('permission_role')
                          ->select('permission_role.*', 'roles.*', 'permissions.*')
                          ->join('roles', 'permission_role.role_id', '=', 'roles.id')
                          ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
                          ->where('roles.slug', $rol[0]->slug)
                          ->get();

              $ability = [];

              foreach ($permisos as $value) {
                $ability[] = ['action'  =>  $value->slug_vue, 'subject' =>  $value->name_module];
              }

              $info[] = [
                "id" => $token->id,
                "id_user"  => $user->id,  //Crypt::encrypt($user->id), 
                "fullName" => $user->name,
                "userName" => $user->name,
                "avatar" => $user->avatar,
                "email" => $user->email,
                "role" => $rol[0]->slug,
                "ability" => $ability
              ];

              $personas = Personas::where('id_usuario', $user->id)->get();
          }

          foreach ($personas as $value) {
            # code...
            $info[1] = $value;
          }

          if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
          }

          $token->save();
       

          return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString(),
            'userData' => $info,
          ]);
    }

    public function logout(Request $request) {
      $request->user()->token()->revoke();
      return response()->json(['message' =>
      'Successfully logged out']);
    }

    public function user(Request $request) {
      return response()->json($request->user());
    }
  
  public function recovery(Request $request){

    
    $validator = Validator::make($request->all(), [
      'email' => 'required|string|email',
    ]);

    if ($validator->fails()) {
      return response()->json(['error_validacion' => $validator->errors()->toJson(), 'code' => 400]);
    }
    
    Password::sendResetLink($credentials);
    
    return response()->json(["msg" => 'Reset password link sent on your email id.']);
  }


}

?>
