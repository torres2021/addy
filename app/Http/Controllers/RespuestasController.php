<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AddyRespuesta as Respuesta;
use App\Models\AddyPregunta as Pregunta;

use App\Models\AddyReferido as Referidos;
use DB;

class RespuestasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   //$GLOBALS['variable'] = something;

        if($request->tipo_cliente == 'cliente_comprador'){
            
            if(empty($request->respuesta1)){
                $respuesta1 = "sin informacion";
                $valor = "0";
            }else{
                $respuesta1 = implode(",", $request->respuesta1);
                $valor  = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta1,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "1",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

            /* =========== RESPUESTA 2 ================ */

            if(empty($request->respuesta2['inicial'])){
                $inicial = 'Sin informacion';
                $valorI = 0;
            }else{
                $inicial  = $request->respuesta2['inicial'];
                $valorI = 5;

            }

            if(empty($request->respuesta2['final'])){
                $final = 'Sin informacion';
                $valorF = 0;

            }else{
                $final  = $request->respuesta2['inicial'];
                $valorF = 5;

            }

            $valor = $valorI+$valorF;
            $respuesta2 =   "Inicial-".$inicial." final-".$final;
            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta2,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "2",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

            /* =========== RESPUESTA 3 ================ */

            if(empty($request->respuesta3['valor'])){
                
                $descripcion="Sin informacion";
                $valor =   "0";

            }else{

                if($request->respuesta3['valor'] == 'no'){
                    $descripcion = $request->respuesta3['valor'];
                    $valor =    "10";
                }else{
                    $descripcion = $request->respuesta3['valor'].", ".$request->respuesta3['subrespuesta'];
                    $valor =    "10";
                }

            }
            
            Respuesta::create([
                'descripcion_respuesta' =>  $descripcion,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "3",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

            /* =========== RESPUESTA 4 ================ */

            if(empty($request->respuesta4['inicial'])){
                $inicial = 'Sin informacion';
                $valorI = 0;
            }else{
                $inicial  = $request->respuesta4['inicial'];
                $valorI = 5;

            }

            if(empty($request->respuesta4['final'])){
                $final = 'Sin informacion';
                $valorF = 0;

            }else{
                $final  = $request->respuesta4['inicial'];
                $valorF = 5;

            }

            $valor = $valorI+$valorF;
            $respuesta4 =   "Inicial-".$inicial." final-".$final;
            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta4,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "4",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);


            /* =========== RESPUESTA 5 ================ */

            
            if(empty($request->respuesta5['valor'])){
                
                $descripcion="Sin informacion";
                $valor =   "0";

            }else{

                if($request->respuesta5['valor'] == 'no'){
                    $descripcion = $request->respuesta5['valor'];
                    $valor =    "10";
                }else{
                    $descripcion = $request->respuesta5['valor'].", ".$request->respuesta5['subrespuesta'];
                    $valor =    "10";
                }

            }
            
            Respuesta::create([
                'descripcion_respuesta' =>  $descripcion,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "5",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

            
            /* =========== RESPUESTA 6 ================ */

            if(!isset($request->respuesta6)){
                $respuesta6 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta6 =   $request->respuesta6;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta6,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "6",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);
            

            /* =========== RESPUESTA 7 ================ */

            if(!isset($request->respuesta7)){
                $respuesta7 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta7 =   $request->respuesta7;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta7,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "7",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);
            

            /* =========== RESPUESTA 8 ================ */

            if(empty($request->respuesta8['valor'])){
                
                $descripcion="Sin informacion";
                $valor =   "0";

            }else{

                if($request->respuesta5['valor'] == 'no'){
                    $descripcion = $request->respuesta8['valor'];
                    $valor =    "10";
                }else{
                    $descripcion = $request->respuesta8['valor'].", ".$request->respuesta8['subrespuesta'];
                    $valor =    "10";
                }

            }
            
            Respuesta::create([
                'descripcion_respuesta' =>  $descripcion,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "8",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);


            /* =========== RESPUESTA 9 ================ */

            if(!isset($request->respuesta9)){
                $respuesta9 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta9 =   $request->respuesta9;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta9,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "9",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

            /* =========== RESPUESTA 10 ================ */

            $suma = DB::table('addy_detalle_pregunta_respuesta_referido')
                    ->select('addy_detalle_pregunta_respuesta_referido.*', 'addy_preguntas.*', 'addy_respuestas.*')
                    ->join('addy_preguntas', 'addy_detalle_pregunta_respuesta_referido.id_pregunta', '=', 'addy_preguntas.id')
                    ->join('addy_respuestas', 'addy_detalle_pregunta_respuesta_referido.id_respuesta', '=', 'addy_respuestas.id')
                    ->where('addy_detalle_pregunta_respuesta_referido.cedula_referido_detalle', $request->id_referido)
                    ->sum('addy_respuestas.valor_respuesta');
            
            $total = ($suma/90) * 100;

            Referidos::where('id', $request->id_referido)->update([
                'porcentaje_perfil' =>   intval($total)."%",
            ]);

        }else if($request->tipo_cliente == 'cliente_vendedor'){
            
            /* RESPUESTA 1 */

            if(!isset($request->respuesta1)){
                $respuesta1 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta1 =   $request->respuesta1;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta1,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "10",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

            /* ========================== RESPUESTA 2 ========================== */

            if(!isset($request->respuesta2)){
                $respuesta2 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta2 =   $request->respuesta2;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta2,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "11",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

             /* ========================== RESPUESTA 3 ========================== */

            if(!isset($request->respuesta3)){
                $respuesta3 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta3 =   $request->respuesta3;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta3,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "12",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

             /* ========================== RESPUESTA 4 ========================== */

            if(!isset($request->respuesta2)){
                $respuesta4 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta4 =   $request->respuesta4;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta4,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "13",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

             /* ========================== RESPUESTA 5 ========================== */

            if(!isset($request->respuesta5)){
                $respuesta5 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta5 =   $request->respuesta5;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta5,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "14",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

             /* ========================== RESPUESTA 6 ========================== */

            if(!isset($request->respuesta2)){
                $respuesta6 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta6 =   $request->respuesta6;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta6,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "15",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

             /* ========================== RESPUESTA 7 ========================== */

            if(!isset($request->respuesta7)){
                $respuesta7 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta7 =   $request->respuesta7;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta7,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "16",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

             /* ========================== RESPUESTA 8 ========================== */

            if(!isset($request->respuesta8)){
                $respuesta8 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta8 =   $request->respuesta8;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta8,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "17",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

             /* ========================== RESPUESTA 9 ========================== */

            if(!isset($request->respuesta2)){
                $respuesta9 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta9 =   $request->respuesta9;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta9,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "18",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);

            /* ========================== RESPUESTA 10 ========================== */

            if(!isset($request->respuesta10)){
                $respuesta10 =  "sin informacion";
                $valor = "0";
            }else{
                $respuesta10 =   $request->respuesta10;
                $valor = "10";
            }

            Respuesta::create([
                'descripcion_respuesta' =>  $respuesta10,
                'valor_respuesta'       =>  $valor,
                'estado_respuesta'      =>  "1",
            ]);

            $respuesta = Respuesta::all()->last();
            
            DB::table('addy_detalle_pregunta_respuesta_referido')->insert([
                'id_pregunta'   =>  "19",
                'id_respuesta'  =>  $respuesta->id,
                'cedula_referido_detalle'   =>  $request->id_referido,
            ]);



            /* ========================== REGISTRO EN BASE DE DATOS ========================== */

             $suma = DB::table('addy_detalle_pregunta_respuesta_referido')
                    ->select('addy_detalle_pregunta_respuesta_referido.*', 'addy_preguntas.*', 'addy_respuestas.*')
                    ->join('addy_preguntas', 'addy_detalle_pregunta_respuesta_referido.id_pregunta', '=', 'addy_preguntas.id')
                    ->join('addy_respuestas', 'addy_detalle_pregunta_respuesta_referido.id_respuesta', '=', 'addy_respuestas.id')
                    ->where('addy_detalle_pregunta_respuesta_referido.cedula_referido_detalle', $request->id_referido)
                    ->sum('addy_respuestas.valor_respuesta');
            
            $total = ($suma/90) * 100;

            Referidos::where('id', $request->id_referido)->update([
                'porcentaje_perfil' =>   intval($total)."%",
            ]);
            
        }else{

        }

        return response()->json([
            "status" => 200,
            "message" => "Registro exitoso"
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
