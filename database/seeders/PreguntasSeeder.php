<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AddyPregunta;


class PreguntasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        AddyPregunta::create([
            'id'    =>  '1',
            'descripcion_pregunta'=> "¿Cuál es la mejor forma para comunicarnos con usted?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '2',
            'descripcion_pregunta'=> "¿En qué horario?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '3',
            'descripcion_pregunta'=> "¿Ha salido con algún agente a ver alguna propiedad?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '4',
            'descripcion_pregunta'=> "¿Cuál es el rango de precio de la compra?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '5',
            'descripcion_pregunta'=> "¿Alguien más participará en la toma de decisión de compra del inmueble?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '6',
            'descripcion_pregunta'=> "¿Quién vivirá o qué uso le dará al inmueble?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '7',
            'descripcion_pregunta'=> "¿Hace cuánto tiempo está buscando este inmueble?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '8',
            'descripcion_pregunta'=> "¿Vive en inmueble alquilado?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '9',
            'descripcion_pregunta'=> "¿Es propietario de algun inmueble?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        /*

        AddyPregunta::create([
            'descripcion_pregunta'=> "¿Cuál es la razón de su compra?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'descripcion_pregunta'=> "¿Hace cuánto tiempo está buscando este inmueble?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'descripcion_pregunta'=> "¿Vive en inmueble alquilado?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        

        AddyPregunta::create([
            'descripcion_pregunta'=> "¿Necesita vender un inmueble actual antes de esta compra?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'descripcion_pregunta'=> "¿Ha firmado un acuerdo de venta con algún agente de bienes raíces para vender su inmueble?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'descripcion_pregunta'=> "¿Quién vivirá o qué uso le dará al inmueble?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'descripcion_pregunta'=> "¿Pagará en efectivo o con préstamo bancario?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'descripcion_pregunta'=> "Cuando tiene crédito con el banco ¿Cuánto cancelará de cuota inicial?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'descripcion_pregunta'=> "¿Le gustaría que le recomendará un asesor bancario de confianza?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);
        
        AddyPregunta::create([
            'descripcion_pregunta'=> "En una escala de 1 a 10, donde 10 significa que debe comprar este inmueble lo antes posible y 1 significa que no está seguro si comprará algo, ¿cómo se calificaría? ____ si no es 10 ¿Qué se requiere para que fueraun 10?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'descripcion_pregunta'=> "¿Para qué fecha requiere su nuevo inmueble?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'descripcion_pregunta'=> "Me encantaría ayudarle a comprar su inmueble, para esto es conveniente que concertemos una cita con todas aquellas personas que tomen la decisión, para poder ayudarles a conseguir lo que necesitan en el tiempo que lo requieran ¿Cuándo le parece bien que nos veamos? Fecha y hora de la cita:",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'descripcion_pregunta'=> "Información adicional",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);
        
        AddyPregunta::create([
            'descripcion_pregunta'=> "¿Hay alguna Información que usted crea que debe permanecer confidencial?",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_comprador',
            'estado_pregunta'=> '1',
        ]);
        */

        /* =============== CLIENTE VENDEDOR ================ */

        AddyPregunta::create([
            'id'    =>  '10',
            'descripcion_pregunta'=> "Tipo de inmueble",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '11',
            'descripcion_pregunta'=> "Direccion",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '12',
            'descripcion_pregunta'=> "Condominio",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '13',
            'descripcion_pregunta'=> "Barrio",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '14',
            'descripcion_pregunta'=> "estrato",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '15',
            'descripcion_pregunta'=> "Año de construccion",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '16',
            'descripcion_pregunta'=> "Area lote",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '17',
            'descripcion_pregunta'=> "Area construida",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '18',
            'descripcion_pregunta'=> "Frente",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '19',
            'descripcion_pregunta'=> "Fondo",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '20',
            'descripcion_pregunta'=> "Numero de pisos",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '21',
            'descripcion_pregunta'=> "Matricula inmobiliaria",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '22',
            'descripcion_pregunta'=> "Servicios públicos Disponibles",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '23',
            'descripcion_pregunta'=> "Valor comercial venta",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '24',
            'descripcion_pregunta'=> "Valor avaluo catastral",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '25',
            'descripcion_pregunta'=> "Valor administracion",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '26',
            'descripcion_pregunta'=> "Saldo hipoteca",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);

        AddyPregunta::create([
            'id'    =>  '27',
            'descripcion_pregunta'=> "Acepta permuta",
            'valor_pregunta'=> '10',
            'slug_modulo'=> 'preguntas',
            'opcional_pregunta'=> 'cliente_vendedor',
            'estado_pregunta'=> '1',
        ]);
    }
}
