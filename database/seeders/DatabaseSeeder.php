<?php

namespace Database\Seeders;

use App\Models\AddyPersona;
use App\Models\AddyPregunta;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call(PaisesInfoSeeder::class);
        $this->call(CiudadesInfoSeeder::class);
        $this->call(PermisosInfoSeeder::class);
        $this->call(ModulosInfoSeeder::class);
        $this->call(TipoClientesInfoSeeder::class);
        $this->call(EmpresaInfoSeeder::class);
        $this->call(PreguntasSeeder::class);
        $this->call(PersonasSeeder::class);
    }

}
