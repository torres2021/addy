<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AddyPersona;

class PersonasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        AddyPersona::create([

	        'cedula_persona'=> '12345678', 
			'tipo_identificacion'=> '1',
			'primer_nombre'=> 'Felipe',
			'segundo_nombre'=> 'Cliente',
			'primer_apellido'=> 'Torres',
			'segundo_apellido'=> 'Velez',
			'correo_persona'=> 'cliente@admin.com',
			'celular_movil'=> '+57 300 123 1212',
			'celular_whatsapp'=> '+57 300 123 1212',
			'codigo_ciudad'=> '12',
			'codigo_pais'=> 'Co',
			'id_perfil'=> '1',
			'id_usuario'=> '2',
			'estado_persona'=> '1',
        
        ]);

        AddyPersona::create([

	        'cedula_persona'	=> '123456789', 
			'tipo_identificacion'=> '1',
			'primer_nombre'=> 'Administrador',
			'segundo_nombre'=> '',
			'primer_apellido'=> 'Administrador',
			'segundo_apellido'=> '',
			'correo_persona'=> 'admin@admin.com',
			'celular_movil'=> '+57 123 456 7891',
			'celular_whatsapp'=> '+57 123 456 7891',
			'codigo_ciudad'=> '12',
			'codigo_pais'=> 'Co',
			'id_perfil'=> '1',
			'id_usuario'=> '1',
			'estado_persona'=> '1',
        
        ]);
               
    }
}
