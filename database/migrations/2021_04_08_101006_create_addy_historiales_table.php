<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyHistorialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_historiales', function (Blueprint $table) {
            $table->id();
            $table->longText('descripcion_historial');
            $table->integer('id_usuario')->length(12)->unsigned()->nullable();
            $table->longText('fecha_registro')->nullable();
            $table->longText('modulo_historial')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_historiales');
    }
}
