<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyDetallePreguntaRespuestaReferidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_detalle_pregunta_respuesta_referido', function (Blueprint $table) {
            $table->id();
            $table->integer('id_pregunta')->length(12)->unsigned()->nullable(false);
            $table->integer('id_respuesta')->length(12)->unsigned()->nullable(false);
            $table->integer('cedula_referido_detalle')->length(12)->unsigned()->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_detalle_pregunta_respuesta_referido');
    }
}
