<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyDetalleModulosEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_detalle_modulos_empresas', function (Blueprint $table) {
            $table->increments('id_detalle_modulo_empresa');
            $table->integer('empresa_id');
            $table->integer('modulo_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_detalle_modulos_empresas');
    }
}
