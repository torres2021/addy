<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddyTipoClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addy_tipo_clientes', function (Blueprint $table) {
            $table->id();
            $table->char('nombre_tipo', 250);
            $table->char('slug_tipo', 150)->unique();
            $table->char('estado_tipo', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addy_tipo_clientes');
    }
}
